<?php echo $header; ?>

<?php

	$titulo = $heading_title;
	$pedido = "Seu pedido foi confirmado!";
	$contate = "Contate-Nos";
	if($titulo == $pedido){
		include_once('conversao_pedido.php');
	} else {
		include_once('conversao_formulario.php');
	}
 ?>

<div id="content">
	
	<!--CONTENT LEFT -->
	<?php echo $column_left; ?>
	
	<!--CONTENT RIGHT -->
	<?php echo $column_right; ?>
	
	<!--PAGE CONTENT WRAPPER -->
	<div class="content-body">
		
		<div class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<span><?php echo $breadcrumb['separator']; ?></span><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
			<?php } ?>
		</div>
		<h1 class="page-heading"><strong><?php echo $heading_title; ?></strong></h1>
		<?php
			if($heading_title == "SUA CONTA FOI CADASTRADA."){
				$heading_title = "Uéééé";?><?php
			}
		?>
		<?php echo $content_top; ?>
		<?php echo $text_message; ?>
		<div class="buttons">
			<div class="right"><a href="<?php echo $continue; ?>" class="button dark-bt"><?php echo $button_continue; ?></a></div>
		</div>
	</div>
	<?php echo $content_bottom; ?>

</div>


				<?php if(isset($minewhat_purchase_tracking_script) && strlen($minewhat_purchase_tracking_script) > 0) { ?>
					<script type="text/javascript">
					<?php echo $minewhat_purchase_tracking_script; ?>
					</script>
				<?php	} ?>
			
<?php echo $footer; ?>