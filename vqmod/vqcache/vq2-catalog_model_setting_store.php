<?php
class ModelSettingStore extends Model {

            // Start of code for Order Delivery Date
            public function editOderDeliveryDateLockOutDateSetting($key, $value, $store_id = 0) {
				//$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = 'order_delivery_date_log'");
				$record_set = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = 'order_delivery_date_log'");
		
				if($record_set->num_rows) {
					if (!is_array($value)) {
						$this->db->query("UPDATE " . DB_PREFIX . "setting SET  `value` = '" . $this->db->escape($value) . "' WHERE store_id = '" . (int)$store_id . "' AND `group` = 'order_delivery_date_log' AND `key` = '" . $this->db->escape($key) . "'");
					} else {
						$this->db->query("UPDATE " . DB_PREFIX . "setting SET  `value` = '" . $this->db->escape(serialize($value)) . "', serialized = '1' WHERE store_id = '" . (int)$store_id . "' AND `group` = 'order_delivery_date_log' AND `key` = '" . $this->db->escape($key) . "'");
					}
				}
				else {
					if (!is_array($value)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `group` = 'order_delivery_date_log', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
					} else {
						$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `group` = 'order_delivery_date_log', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(serialize($value)) . "', serialized = '1'");
					}
				}
				 
			}
            // End of code for Order Delivery Date
            
	public function getStores($data = array()) {
		$store_data = $this->cache->get('store');
	
		if (!$store_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store ORDER BY url");

			$store_data = $query->rows;
		
			$this->cache->set('store', $store_data);
		}
	 
		return $store_data;
	}	
}
?>