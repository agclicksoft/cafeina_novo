<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_install) { ?>
  <div class="warning"><?php echo $error_install; ?></div>
  <?php } ?>
  <?php if ($error_image) { ?>
  <div class="warning"><?php echo $error_image; ?></div>
  <?php } ?>
  <?php if ($error_image_cache) { ?>
  <div class="warning"><?php echo $error_image_cache; ?></div>
  <?php } ?>
  <?php if ($error_cache) { ?>
  <div class="warning"><?php echo $error_cache; ?></div>
  <?php } ?>
  <?php if ($error_download) { ?>
  <div class="warning"><?php echo $error_download; ?></div>
  <?php } ?>
  <?php if ($error_logs) { ?>
  <div class="warning"><?php echo $error_logs; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/home.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content">
      <div class="overview">
        <div class="dashboard-heading"><?php echo $text_overview; ?></div>
        <div class="dashboard-content">
          <table>
            <tr>
              <td><?php echo $text_total_sale; ?></td>
              <td><?php echo $total_sale; ?></td>
            </tr>
            <tr>
              <td><?php echo $text_total_sale_year; ?></td>
              <td><?php echo $total_sale_year; ?></td>
            </tr>
            <tr>
              <td><?php echo $text_total_order; ?></td>
              <td><?php echo $total_order; ?></td>
            </tr>
            <tr>
              <td><?php echo $text_total_customer; ?></td>
              <td><?php echo $total_customer; ?></td>
            </tr>
            <tr>
              <td><?php echo $text_total_customer_approval; ?></td>
              <td><?php echo $total_customer_approval; ?></td>
            </tr>
            <tr>
              <td><?php echo $text_total_review_approval; ?></td>
              <td><?php echo $total_review_approval; ?></td>
            </tr>
            <tr>
              <td><?php echo $text_total_affiliate; ?></td>
              <td><?php echo $total_affiliate; ?></td>
            </tr>
            <tr>
              <td><?php echo $text_total_affiliate_approval; ?></td>
              <td><?php echo $total_affiliate_approval; ?></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="statistic">
        <div class="range"><?php echo $entry_range; ?>
          <select id="range" onchange="getSalesChart(this.value)">
            <option value="day"><?php echo $text_day; ?></option>
            <option value="week"><?php echo $text_week; ?></option>
            <option value="month"><?php echo $text_month; ?></option>
            <option value="year"><?php echo $text_year; ?></option>
          </select>
        </div>
        <div class="dashboard-heading"><?php echo $text_statistics; ?></div>
        <div class="dashboard-content">
          <div id="report" style="width: 390px; height: 170px; margin: auto;"></div>
        </div>
      </div>
      <div class="latest">
        <div class="dashboard-heading"><?php echo $text_latest_10_orders; ?></div>
        <div class="dashboard-content">
          <table class="list">
            <thead>
              <tr>
                <td class="right"><?php echo $column_order; ?></td>
                <td class="left"><?php echo $column_customer; ?></td>
                <td class="left"><?php echo $column_status; ?></td>
                <td class="left"><?php echo $column_date_added; ?></td>
                <td class="right"><?php echo $column_total; ?></td>

            <!-- Start of code for Order Delivery Date -->
            <?php if($this->config->get('order_delivery_date_status')){  ?>
            	<td class="left"><?php echo $text_oder_delivery_date_label; ?></td>
            <?php } ?>
            <!-- End of code for Order Delivery Date -->
            
                <td class="right"><?php echo $column_action; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php if ($orders) { ?>
              <?php foreach ($orders as $order) { ?>
              <tr>
                <td class="right"><?php echo $order['order_id']; ?></td>
                <td class="left"><?php echo $order['customer']; ?></td>
                <td class="left"><?php echo $order['status']; ?></td>
                <td class="left"><?php echo $order['date_added']; ?></td>
                <td class="right"><?php echo $order['total']; ?></td>

            <!-- Start of code for Order Delivery Date -->
            <?php if($this->config->get('order_delivery_date_status')){  ?>
            	<td class="left">
            		<?php 
            			$temp = explode('@ Order Delivery Date', $order['comment']);
            			if(count($temp)>1) {
            				echo $order_delivery_date = $temp[count($temp)-1];
            			}
            			else '-';
            		?>		
            	</td>
            <?php } ?>
            <!-- End of code for Order Delivery Date -->
            
                <td class="right"><?php foreach ($order['action'] as $action) { ?>
                  [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>

			<?php if ( isset($config_oo_api_key) AND isset($config_oo_profile_id) ) { ?>
			<br />
			<div class="latest">
					<div class="dashboard-heading"><?php echo $text_ga_summary; ?></div>
					<div class="dashboard-content">       
					
					<div id='chart_visits'></div>      
					<div id='chart_pageviews'></div>
					
					<table id='pie_table' width="90%" cellpadding="10" align="center">
					  <tr>
						<td width="65%"><div id='zone'></div></td>
						<td width="35%"><div id='pie'></div></td>
					  </tr>          
					</table>        
					
					<table id='list_table' width="90%" cellpadding="10" align="center">
					  <tr>
						<td width="45%"><div id='list_searches'></div></td>
						<td width="45%"><div id='list_referers'></div></td>
					  </tr>
					</table>

					<div style="text-align:right;"><a href="https://www.google.com/analytics/web/?hl=cs&pli=1#home/">Google Analytics</a></div>
					
					<script type="text/javascript">

						window.onload = function(){
							
							oo.setAPIKey("<?php echo $config_oo_api_key; ?>");

							oo.load(function(){

								// Set Parameters
								var profile_id = "<?php echo $config_oo_profile_id; ?>"
								  , date_days = "30d"
								  , max_result = <?php echo ( isset($config_oo_maxresult) ? $config_oo_maxresult : 10 ); ?>      // 10 | 20 | ...
								  , max_result_zone = 10 // 10 | 20 | ...
								  , timeline_display = <?php echo ($config_oo_timeline == 1 ? 'true' : 'false'); ?> // true | false
								  , pie_display      = <?php echo ($config_oo_bar_pie == 1 ? 'true' : 'false'); ?> // true | false
								  , table_display    = <?php echo ($config_oo_table == 1 ? 'true' : 'false'); ?> // true | false
								  ;
								
								if ( timeline_display ) {
								
								  // Visits & Vistors
								  var tl1 = new oo.Timeline(profile_id, date_days);
								  tl1.addMetric("ga:visits", "<?php echo $text_ga_visits; ?>");
								  tl1.addMetric("ga:visitors", "<?php echo $text_ga_visitors; ?>");
								  tl1.draw('chart_visits');
								  
								  // PageViews
								  var tl2 = new oo.Timeline(profile_id, date_days);
								  tl2.addMetric("ga:pageviews", "<?php echo $text_ga_pageviews; ?>"); 
								  tl2.draw('chart_pageviews');
								  
								}
								
								if ( pie_display ) {
								  
								  var div_height = '400px';
								  
								  // Zone
								  document.getElementById("zone").style.height = div_height;
								  var bar = new oo.Bar(profile_id, date_days);
								  bar.addMetric("ga:visits", "<?php echo $text_ga_visits; ?>");
								  bar.addMetric("ga:newVisits", "<?php echo $text_ga_new_visits; ?>");
								  bar.setDimension("ga:region");
								  bar.query.setSort('-ga:visits');
								  bar.query.setMaxResults(max_result_zone);
								  bar.draw('zone');                      
								  
								  // Visits vs New Visits
								  document.getElementById("pie").style.height = div_height;
								  var p2 = new oo.Pie(profile_id, date_days);
								  p2.setMetric("ga:visits", "<?php echo $text_ga_visits; ?>");
								  p2.setDimension("ga:visitorType");
								  p2.draw('pie');
								}
								
								if ( table_display ) {
								
								  // Searches                    
								  var t1 = new oo.Table(profile_id, date_days);                    
								  t1.addDimension("ga:keyword", "<?php echo $text_ga_top_searches; ?>"); // klicova slova
								  t1.addMetric("ga:visits", "<?php echo $text_ga_visits; ?>");
								  //t1.addMetric("ga:searchResultViews", "ResultViews");                    
								  t1.addMetric("ga:newVisits", "<?php echo $text_ga_new_visits; ?>");
								  //t1.query.setFilter("ga:searchResultViews>0");                     
								  t1.query.setSort('-ga:visits');
								  t1.query.setMaxResults(max_result);
								  t1.draw('list_searches');                    
								  
								  // Referers                    
								  var t2 = new oo.Table(profile_id, date_days);
								  t2.addDimension("ga:source", "<?php echo $text_ga_top_referers; ?>"); // odkazujici stranky
								  t2.addMetric("ga:visits", "<?php echo $text_ga_visits; ?>");  // veskera navstevnost
								  t2.addMetric("ga:pageviews", "<?php echo $text_ga_pageviews; ?>");                   
								  t2.query.setFilter("ga:medium==referral"); // odkazujici stranky                    
								  t2.query.setSort('-ga:pageviews');
								  t2.query.setMaxResults(max_result);
								  t2.draw('list_referers');                    
								  
								} else {
								
								  // document.getElementById("pie_table").style.display = 'none';
								  // document.getElementById("list_table").style.display = 'none';
								
								}
								
							});
							
						};
					
					</script>
			  </div>
			</div>
			<br />
			<?php } ?>      

			<script type="text/javascript">$('#range').val('month');</script>
      
			
    </div>
  </div>
</div>
<!--[if IE]>
<script type="text/javascript" src="view/javascript/jquery/flot/excanvas.js"></script>
<![endif]--> 
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.js"></script> 
<script type="text/javascript"><!--
function getSalesChart(range) {
	$.ajax({
		type: 'get',
		url: 'index.php?route=common/home/chart&token=<?php echo $token; ?>&range=' + range,
		dataType: 'json',
		async: false,
		success: function(json) {
			var option = {	
				shadowSize: 0,
				lines: { 
					show: true,
					fill: true,
					lineWidth: 1
				},
				grid: {
					backgroundColor: '#FFFFFF'
				},	
				xaxis: {
            		ticks: json.xaxis
				}
			}

			$.plot($('#report'), [json.order, json.customer], option);
		}
	});
}

getSalesChart($('#range').val());
//--></script> 
<?php echo $footer; ?>