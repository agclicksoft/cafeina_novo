<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>

<?php if(empty($shipping_methods)) { ?>
  <div class="warning">Não há entrega Delivery no Cep Informado!</div>
<?php } else {?>


<script type="text/javascript">
    $(".retirarabre").click(function(){
      $("#selecionar_retirada").show("slow");
      $("#info-entrega").css("display","none");
      $("#info-retirar").css("display","block");
    }); 

    $(".retirarfecha").click(function(){
      $("#selecionar_retirada").hide("slow");
      $("#info-entrega").css("display","block");
      $("#info-retirar").css("display","none");
       $("#lojasel").val(null);
    });    
  </script>

<?php if ($shipping_methods) { ?>
<div class="box-form shipping-method">
	<p><?php echo $text_shipping_method; ?></p>	
	<table class="radio">
		<?php foreach ($shipping_methods as $shipping_method) { ?>
		<tr>
			<td colspan="3" class="shipping-name"><b><?php echo $shipping_method['title']; ?></b></td>
		</tr>
		<?php if (!$shipping_method['error']) { ?>
		<?php foreach ($shipping_method['quote'] as $quote) { ?>
		<tr>
			<td><?php if ($quote['code'] == $code || !$code) { ?>
				<?php $code = $quote['code']; ?>
        <?php if($category_id_product_cart != 60) {?>
				<input type="radio" class="retirarabre" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>"  />
        <?php } else { ?>
        <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>"  />
        <?php }  ?>
				<?php } else { ?>
				<input type="radio" class="retirarfecha" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
				<?php } ?></td>
			<td><label for="<?php echo $quote['code']; ?>"><?php echo $quote['title']; ?></label></td>
			<td style="text-align: right;" class="price"><label for="<?php echo $quote['code']; ?>"><?php echo $quote['text']; ?></label></td>
		</tr>
		<?php } ?>
		<?php } else { ?>
		<tr>
			<td colspan="3" class="shipping-name"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
		</tr>
		<?php } ?>
		<?php } ?>
	</table>

  <div id="selecionar_retirada" style="display:none;">
      <input class="loja" type="radio" name="loja" value="Copa 1" checked="checked">Copa 1<br/>
      <input class="loja" type="radio" name="loja" value="Copa 2"/>Copa 2<br/>
      <input class="loja" type="radio" name="loja" value="Ipanema"/>Ipanema<br/>
      <input class="loja" type="radio" name="loja" value="Leblon"/>Leblon<br/>
      <input class="loja" type="radio" name="loja" value="Botafogo"/>Botafogo<br/>
      <input class="loja" type="radio" name="loja" value="Nova América"/>Nova América<br/>
      <input class="loja" type="radio" name="loja" value="Rio Sul"/>Rio Sul<br/>
  </div>
<input type="hidden" name="lojasel" id="lojasel">
  
  <script type="text/javascript">
    $(".loja").click(function(){
      $("#lojasel").val($(this).val());
    });
  </script>

<?php } ?>
  <?php if($category_id_product_cart != 60) {?>

<div id="datahoraenvio">
 <br><div id="info-entrega">
 <br><br><div><?php echo "Selecione a Data e Hora de Entrega:" ?></div>
 <div style="color:red;"><?php echo "* Agendamento da entrega é de no mínimo 24 horas!" ?></div>
 <br><br></div>

 <br><div id="info-retirar" style="display:none;">
 <br><br><div><?php echo "Selecione a Data e Hora de Retirada:" ?></div>
 <br><br></div>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
 <script>

  /*
  12/10/15- Nossa Senhora
  02/11/15- Finados
  15/11/15- Proclamação da República
  25/12/15- Natal
  01/01/16 - Ano novo
  */

  /* create an array of days which need to be disabled */
  //var disabledDays = ["10-12-2015","11-2-2015","11-15-2015","12-25-2015","1-1-2016"];
  var disabledDays = ["12-25-2015"];

  /* utility functions */
  function nationalDays(date) {
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
    //console.log('Checking (raw): ' + m + '-' + d + '-' + y);
    for (i = 0; i < disabledDays.length; i++) {
      if($.inArray((m+1) + '-' + d + '-' + y,disabledDays) != -1 || new Date() > date) {
        //console.log('bad:  ' + (m+1) + '-' + d + '-' + y + ' / ' + disabledDays[i]);
        return [false];
      }
    }
    //console.log('good:  ' + (m+1) + '-' + d + '-' + y);
    return [true];
  }
  function noWeekendsOrHolidays(date) {
    var noWeekend = jQuery.datepicker.noWeekends(date);
    //return noWeekend[0] ? nationalDays(date) : noWeekend;
    return nationalDays(date);
  }

  $(function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'dd/mm/yy',
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
      //numberOfMonths: 2,
      minDate: + 1,
      beforeShowDay: noWeekendsOrHolidays
    });
  });
 </script>

 <script type="text/javascript">

  $(function(){
   //Função que ao clicar no botão, irá fazer.

   $("#verificar").click(function(){
    var datepicker = $("#datepicker").val();
    var hora = $("#hora").val();
    var minuto = $("#minuto").val();

    //alert(datepicker);


    // inicio uma requisição
    $.ajax({
     // url para o arquivo json.php
     url : "catalog/controller/module/data_hora.php",
     //Método de envio
     type: 'POST',
     // dataType json
     dataType : "json",

     data : { 'datepicker':datepicker, 'hora':hora, 'minuto':minuto },
     // função para de sucesso
     success : function(data){

      //console.log(data);
      if(data['Status'] == '0'){

       $("#retorno").show("slow").text("Você precisa selecionar uma data.");
       setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
       $("#buttoncont").hide("slow");
       $("#selecioneenvio").show("slow");
      } else if(data['Status'] == '1') {
       $("#retorno").show("slow").text("Horário mínimo permitido para o dia "+data['datepicker'] + " é às " + data['hora']);
       setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
       $("#buttoncont").hide("slow");
       $("#selecioneenvio").show("slow");
      } else {
       $("#retorno").show("slow").text("Sucesso! Data e Hora de Entrega: "+data['datepicker'] + " às " + data['hora']);
       setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
       $("#buttoncont").show("slow");
       $("#selecioneenvio").hide("slow");
       var data_hora = data['datepicker'] + " " + data['hora'];
       //console.log(data_hora);
       $("#data_hora").val(data_hora);
     }
     }
    });//termina o ajax
   });


  });
 </script>

 <div id="formulario">
  <input type="hidden" name="data_hora" id="data_hora">
   <?php echo "Data:"; ?><input type="text" name="datepicker" id="datepicker"><br/><br/>
   <?php echo "Hora: "; ?> 
   <select name="hora" id="hora">
    <option value="08"><?php echo "08"; ?></option> 
    <option value="09"><?php echo "09"; ?></option>
    <option value="10"><?php echo "10"; ?></option>
    <option value="11"><?php echo "11"; ?></option> 
    <option value="12"><?php echo "12"; ?></option>
    <option value="13"><?php echo "13"; ?></option>
    <option value="14"><?php echo "14"; ?></option>
    <option value="15"><?php echo "15"; ?></option>
    <option value="16"><?php echo "16"; ?></option>
    <option value="17"><?php echo "17"; ?></option>
   </select>
   <select name="minuto" id="minuto">
    <option value="00"><?php echo "00"; ?></option>
    <option value="01"><?php echo "01"; ?></option> 
    <option value="02"><?php echo "02"; ?></option> 
    <option value="03"><?php echo "03"; ?></option> 
    <option value="04"><?php echo "04"; ?></option> 
    <option value="05"><?php echo "05"; ?></option>
    <option value="06"><?php echo "06"; ?></option> 
    <option value="07"><?php echo "07"; ?></option> 
    <option value="08"><?php echo "08"; ?></option> 
    <option value="09"><?php echo "09"; ?></option>
    <option value="10"><?php echo "10"; ?></option>
    <option value="11"><?php echo "11"; ?></option> 
    <option value="12"><?php echo "12"; ?></option> 
    <option value="13"><?php echo "13"; ?></option> 
    <option value="14"><?php echo "14"; ?></option>
    <option value="15"><?php echo "15"; ?></option> 
    <option value="16"><?php echo "16"; ?></option> 
    <option value="17"><?php echo "17"; ?></option> 
    <option value="18"><?php echo "18"; ?></option> 
    <option value="19"><?php echo "19"; ?></option>
    <option value="20"><?php echo "20"; ?></option>
    <option value="21"><?php echo "21"; ?></option> 
    <option value="22"><?php echo "22"; ?></option> 
    <option value="23"><?php echo "23"; ?></option> 
    <option value="24"><?php echo "24"; ?></option>
    <option value="25"><?php echo "25"; ?></option>
    <option value="26"><?php echo "26"; ?></option> 
    <option value="27"><?php echo "27"; ?></option> 
    <option value="28"><?php echo "28"; ?></option> 
    <option value="29"><?php echo "29"; ?></option>
    <option value="30"><?php echo "30"; ?></option>
    <option value="31"><?php echo "31"; ?></option> 
    <option value="32"><?php echo "32"; ?></option> 
    <option value="33"><?php echo "33"; ?></option> 
    <option value="34"><?php echo "34"; ?></option>
    <option value="35"><?php echo "35"; ?></option>
    <option value="36"><?php echo "36"; ?></option> 
    <option value="37"><?php echo "37"; ?></option> 
    <option value="38"><?php echo "38"; ?></option> 
    <option value="39"><?php echo "39"; ?></option>
    <option value="40"><?php echo "40"; ?></option>
    <option value="41"><?php echo "41"; ?></option> 
    <option value="42"><?php echo "42"; ?></option> 
    <option value="43"><?php echo "43"; ?></option> 
    <option value="44"><?php echo "44"; ?></option>
    <option value="45"><?php echo "45"; ?></option>
    <option value="46"><?php echo "46"; ?></option> 
    <option value="47"><?php echo "47"; ?></option>
    <option value="48"><?php echo "48"; ?></option> 
    <option value="49"><?php echo "49"; ?></option>
    <option value="50"><?php echo "50"; ?></option>
    <option value="51"><?php echo "51"; ?></option> 
    <option value="52"><?php echo "52"; ?></option> 
    <option value="53"><?php echo "53"; ?></option> 
    <option value="54"><?php echo "54"; ?></option>
    <option value="55"><?php echo "55"; ?></option>
    <option value="56"><?php echo "56"; ?></option> 
    <option value="57"><?php echo "57"; ?></option> 
    <option value="58"><?php echo "58"; ?></option> 
    <option value="59"><?php echo "59"; ?></option>
   </select>

   <br/><br/>

   <input type="button" name="verificar" id="verificar" value="Selecionar" class="button" />
 </div>

 <!-- Parte onde carrega a imagem -->
 <div id="carregando_form" style="text-align:center; display:none;">Carregando...</div>

 <!-- Aqui você muda a parte visual da mensagem -->

            <?php
            	// Start of code for Order Delivery Date 
            	
            	
            	if ($order_delivery_date_status) { 
            	
                
	            $show = 'datepicker';
	            $field_name = 'order_delivery_date';
	            
            	echo '<script type="text/javascript" src="./catalog/view/javascript/jquery/ui/minified/jquery.ui.datepicker.min.js"></script>';
            	
	            	echo '<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/smoothness/jquery-ui.css?ver=3.4.2" type="text/css" media="" />';
	            
	           
	        ?>     
	        	<style>
	        		.odd-title{margin-bottom:2px;display:block;}
	        		.odd-box{background:#F7F7F7;border:1px solid #DDDDDD;padding:10px;margin-bottom:10px;}
	        	</style>
	             <div class='odd-title'>Delivery Date</div>
	             <div class='odd-box'>
	               <p>We will try to deliver the order on the specified delivery date</p>
	               <table width='auto' cellpadding='3' cellspacing='3' border='0'>
	                 <tr>
	                   <td align='left' valign='middle'>Delivery Date</td>
	                   <td align='left' valign='middle'><input type='text' name='order_delivery_date' id='order_delivery_date' value='<?php echo $order_delivery_date; ?>' size='40' class='order_delivery_date' readonly='readonly'/></td>
	                 </tr>
	               </table>
	             </div>
	         <?php    
	            
	            
	            $display = '';
				$minDate = 0;
					            
	            				
					$options_free[] = "dateFormat: \"d MM, yy\"";
					$options_free[] = "minDate: \"1\"";
					$options_free[] = "maxDate: \"30\"";
				
				$options_str = implode(',',$options_free);
					
				$display_datepicker = 'jQuery("#'.$field_name.'").val("").'.$show.'({'.$options_str.'})
				.focus(function (event){
					jQuery.datepicker.afterShow(event);
				});';
				
				
				
				
				$display .= '
				<script type="text/javascript">';
					
					$display .= '
					jQuery(document).ready(function(){
						var formats = ["d.m.y", "d MM, yy","MM d, yy"];
						$.extend($.datepicker, { afterShow: function(event){
							$.datepicker._getInst(event.target).dpDiv.css("z-index", 99);
						}});
						'.$display_datepicker;
						$display .= '
					});
					
					
			    </script>';
				echo $display;
			 }
			 // End of code for Order Delivery Date 
			 ?>
            
 <div id="retorno" style="font-family:Calibri, 'Trebuchet MS', Verdana; font-size: 15px; border: 1px solid #000; background: #F5F5F5; text-align: center; display:none; padding: 10px 10px 10px 10px;"><br /><br/></div>

</div>


<br><br>
<b><?php echo $text_comments; ?></b>
<textarea name="comment" rows="8" style="width: 98%;"><?php echo $comment; ?></textarea>
<br />
<br />

<div id="selecioneenvio" class="warning" style="display:block;"><?php echo "Atenção! Para continuar, selecione a data e hora!" ?></div>

<div class="buttons" id="buttoncont" style="display:none;">
  <div class="right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" class="button" />
  </div>
</div>

<?php } else {?>

<script type="text/javascript">
 $(function(){
  $("#buttoncont").show("slow");
  $("#selecioneenvio").hide("slow");
 })
</script>
<div style="color:red;"><?php echo "* Os pedidos de delivery têm prazo de entrega de 60 minutos a partir da aprovação do pagamento!" ?></div><br>
<b><?php echo $text_comments; ?></b>
<textarea name="comment" rows="8" style="width: 98%;"><?php echo $comment; ?></textarea>
<br />
<br />

<div id="selecioneenvio" class="warning" style="display:none !important;"><?php echo "Para continuar, selecione a data e hora de envio!" ?></div>

<div class="buttons" id="buttoncont" style="display:block !important;">
  <div class="right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" class="button" />
  </div>
</div>
<?php }?>
<?php } ?>