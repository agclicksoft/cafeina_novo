<?php
$current_directory = getcwd();
$admin_dir_path = $current_directory.'/../admin/';
$vqmod_dir_path = $current_directory.'/../vqmod/';
$opencart_dir_path = $current_directory.'/../../';
$opencart_admin_dir_path = $current_directory.'/../../admin/';
$opencart_vqmod_dir_path = $current_directory.'/../../vqmod/';

$admin_dir_array = array(
						'controller/module/',
						'language/english/module/',
						'view/template/module/',
						'view/image/',
						
					);

// check all folders exists
if(!checkDirExists($opencart_vqmod_dir_path)){
	die("Error: vqmod folder not found. Please install vqmod lib first");
}			
foreach ($admin_dir_array as $admin_dir){
	// check all folders exists
	if(!checkDirExists($admin_dir_path.$admin_dir)){
		die("Error: $admin_dir folder not found.");
	}
}
 
// Copy vqmod xml file
if (!copy($vqmod_dir_path.'xml/vqmod_order_delivery_date.xml', $opencart_vqmod_dir_path.'xml/vqmod_order_delivery_date.xml')) {
	die("failed to copy ".$opencart_vqmod_dir_path."xml/vqmod_order_delivery_date.xml");
}
@chmod($opencart_vqmod_dir_path.'xml/vqmod_order_delivery_date.xml', 0755);

// Copy Admin files and folder
foreach ($admin_dir_array as $admin_dir){
	readAndCopyDirFiles($admin_dir_path.$admin_dir, $opencart_admin_dir_path.$admin_dir);
}

echo 'The Order Delivery Date extension has been successfully setup. You can now install the extension from "Extensions -> Modules" from the OpenCart Administration panel. Once installed, please delete the folder "order_delivery_date_lite_installer" present in your opencart root folder.';

function readAndCopyDirFiles($source_dir, $destination){
	
	if ($dh = opendir($source_dir)) {
    	 while (($file = readdir($dh)) !== false) {
            if($file!='.' && $file!='..' && $file!='Thumbs.db'){
            	if(filetype($source_dir.$file)=='dir'){
            		if(!file_exists($destination.$file)) {
            			if(!mkdir($destination.$file)){
            				die('Failed to create directory '.$destination.$file);
            			}
            			@chmod($destination.$file, 0755);
            		}
            		readAndCopyDirFiles($source_dir.$file.'/', $destination.$file.'/');
            	}
            	else { 
					if (!copy($source_dir.$file, $destination.$file)) {
    					die("failed to copy $file");
					}
					@chmod($destination.$file, 0755);
				}
            }
    	 }
    }
    else {
    	die("Error: Admin folder is not readable.");
    }
}

function checkDirExists($dir){
	if (is_dir($dir)) return true;   
	else return false;
}


?>