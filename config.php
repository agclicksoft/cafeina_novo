<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/cafeina/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/cafeina/');

// DIR
define('DIR_APPLICATION', '/var/www/html/cafeina/catalog/');
define('DIR_SYSTEM', '/var/www/html/cafeina/system/');
define('DIR_DATABASE', '/var/www/html/cafeina/system/database/');
define('DIR_LANGUAGE', '/var/www/html/cafeina/catalog/language/');
define('DIR_TEMPLATE', '/var/www/html/cafeina/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/html/cafeina/system/config/');
define('DIR_IMAGE', '/var/www/html/cafeina/image/');
define('DIR_CACHE', '/var/www/html/cafeina/system/cache/');
define('DIR_DOWNLOAD', '/var/www/html/cafeina/download/');
define('DIR_LOGS', '/var/www/html/cafeina/system/logs/');

// DB
define('DB_DRIVER', 'mmysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'cafeina_oc');
define('DB_PREFIX', 'oc_');
?>