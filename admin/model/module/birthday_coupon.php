<?php
class ModelModuleBirthdayCoupon extends Model {
	public function createTables() {
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX .	"customer_birthday` (
				  `customer_id` int(11) NOT NULL,
				  `year` int(11) NOT NULL,
				  `month` int(11) NOT NULL,
				  `day` int(11) NOT NULL
				) DEFAULT CHARSET=utf8;";
		
		$this->db->query($sql);

		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customer_birthday_history` (
				  `customer_id` int(11) NOT NULL,
				  `year` int(11) NOT NULL,
				  `code` varchar(10) NOT NULL,
				  `type` varchar(10) NOT NULL,
				  `discount` decimal(15,4) NOT NULL,
				  `total` decimal(15,4) NOT NULL,
				  `validity` datetime NOT NULL,
				  `date_added` datetime NOT NULL
				) DEFAULT CHARSET=utf8;";
				
		$this->db->query($sql);
	}
}
?>