<?php
// Heading
$_['heading_title']       		  = 'Birthday Coupon';

// Tab
$_['tab_general']         		  = 'General Settings';
$_['tab_email_template']          = 'Email Template';
$_['tab_coupon']                  = 'Coupon Settings';
$_['tab_help']           		  = 'Help';

// Text
$_['text_module']         		  = 'Modules';
$_['text_success']        		  = 'Success: You have modified module Birthday Coupon!';
$_['text_content_top']    		  = 'Content Top';
$_['text_content_bottom'] 		  = 'Content Bottom';
$_['text_column_left']    		  = 'Column Left';
$_['text_column_right']           = 'Column Right';
$_['text_up_right']               = 'Corner Up-Right';
$_['text_down_right']             = 'Corner Down-Right';
$_['text_down_left']              = 'Corner Down-Left';
$_['text_up_left']                = 'Corner Up-Left';

$_['text_percent']                = 'Percent';
$_['text_fixed']                  = 'Fixed';

// Entry
$_['entry_secret_code']           = 'Cron secret code:<span class="help">Use characters a-z,A-Z,0-9<br />set cron to run once/day</span>';
$_['entry_admin_report']          = 'Send report to admin?<span class="help">an email is sent to admin when cron job send birthday message to customers</span>';
$_['entry_allow_update']          = 'Allow update birthday?';
$_['entry_badge_position']        = 'Badge Position:';
$_['entry_badge_color']           = 'Badge Color:';
$_['entry_auto_open_times']       = 'Auto open popup (times):<span class="help">how many times to force auto open popup?</span>';

$_['entry_layout']                = 'Layout:';
$_['entry_position']              = 'Position:';
$_['entry_status']                = 'Status:';
$_['entry_sort_order']            = 'Sort Order:';

$_['entry_email_subject']         = 'Email subject:';
$_['entry_email_message']         = 'Email message:<span class="help"><br />Special Keyword:<br />{firstname} = customer firstname<br />{lastname} = customer lastname<br />{coupon_code} = coupon code<br />{discount} = discount value for coupon<br />{total_amount} = total amount required to activate coupon<br />{validity_days} = how many days coupon is valid<br />{store_name} = store name</span>';

$_['entry_type']                  = 'Type:<br /><span class="help">Percentage or Fixed Amount</span>';
$_['entry_discount']              = 'Discount:';
$_['entry_total']                 = 'Total Amount:<br /><span class="help">The total amount that must reached before the coupon is valid.</span>';
$_['entry_validity_days']         = 'Validity (days):<span class="help">how many days coupon is valid</span>';

// Error
$_['error_permission']    		  = 'Warning: You do not have permission to modify module Birthday Coupon!';
$_['error_secret_code']    		  = 'Error: Secret code is required for cron job!';
$_['error_discount']    		  = 'Error: Discount value is required!';
$_['error_validity_days']         = 'Error: You have to specify how many days discount coupon is valid!';
$_['error_email_subject']         = 'Error: Birthday email subject is required!';
$_['error_email_message']         = 'Error: Birthday email message is required!';
?>