<?php
// Heading
$_['heading_title']       		                          = 'Order Delivery Date Lite';
// Tab
$_['tab_general_settings']                                 = 'General Settings';
// Tab Heading
$_['heading_tab_general_settings']                         = 'General Settings';
// Button

// Text
$_['text_module']         		                           = 'Modules';
$_['text_success']        		                           = 'Success: You have modified module Order Delivery Date!';
// Entry
$_['entry_status']        		                           = 'Status:';
$_['tool_tip_status']        		                       = 'If you want to deactivate the extension, select "Disabled" from the dropdown & click on "Save".';
//info
//conf
$_['conf_before_delete']    		                       = 'Are you sure you want to delete it?';
// Error
$_['error_permission']    		                           = 'Warning: You do not have permission to modify module Order Delivery Date 1.0';
$_['error_text']        		                           = 'Error: if you choose to display custom text to be completed in all languages!';



?>