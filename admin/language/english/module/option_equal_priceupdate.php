<?php
// Heading
$_['heading_title']						= '<b>Multi Price with dynamic price update</b>';
$_['text_module']						= 'Modules';

// Text
$_['txt_option_equal_enableopupdate']	= 'Enable Update total<span class="help">The total and the plan will be updated value when the options are selected</span>';
$_['txt_option_equal_ocultpriceop']		= 'Ocultar preço nas opções<span class="help">the price (+4,00 ou -4,00) next to each option will be hidden</span>';
$_['txt_option_ocultprefix']			= 'Ocultar prefixo dos preços nas opções<span class="help">the prefix (+ ou -) off the price of each option will be hidden</span>';
$_['txt_option_equal_enableanimation']	= 'Ativar Animação<span class="help">will enable the animation of the price</span>';
$_['txt_option_equal_alter_price']		= 'Replace price per<span class="help">Put something in place of the price, so the price will only appear when the customer selects an option, but want to mess place -1</span>';
$_['txt_option_equal_css']				= 'CSS for the price<span class="help">If you want to put some css to style the price</span>';

// Error
$_['error_permission']					= 'Warning: You do not have permission to modify this module';

//
$_['checking_update_text']				= 'Checking for new updates';

?>