<?php
// Heading
$_['heading_title']						= '<b>MultiPreço com atualização dinâmica de preços</b>';
$_['text_module']						= 'Módulos';

// Text
$_['txt_option_equal_enableopupdate']	= 'Ativar atualização de total<span class="help">O valor total será atualizado quando as opções forem selecionadas</span>';
$_['txt_option_equal_ocultpriceop']		= 'Ocultar preço nas opções<span class="help">o preço (+4,00 ou -4,00) ao lado de cada opção será ocultado</span>';
$_['txt_option_ocultprefix']			= 'Ocultar prefixo dos preços nas opções<span class="help">o prefixo (+ ou -) ao lado do preço de cada opção será ocultado</span>';
$_['txt_option_equal_enableanimation']	= 'Ativar Animação<span class="help">Irá habilitar a animação do preço</span>';
$_['txt_option_equal_alter_price']		= 'Substituir preço por<span class="help">Coloque algo no lugar do preço, assim o preço só irá aparecer quando o cliente selecionar uma opção, senão quiser mexer coloque -1</span>';
$_['txt_option_equal_css']				= 'CSS para o preço<span class="help">Se quiser colocar algum css para estilizar o preço</span>';

// Error
$_['error_permission']					= 'Atenção: Você não possui permissão para modificar este módulo';

//
$_['checking_update_text']				= 'Verificando se existem novas atualizações';

?>