<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if(isset($error_warning)) { ?>
<div class="warning">
<?php echo $error_warning; ?>
</div>
<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/payment.png"> <?php echo $heading_title; ?></h1>
			<div class="buttons">
				<a onclick="$('#form').submit();" class="button btn btn-primary"> <span><?php echo $button_save; ?></span></a>
				<a onclick="location = '<?php echo $cancel; ?>';" class="button btn btn-primary"><span><?php echo $button_cancel; ?></span></a>
			</div>
		</div>
		<div class="content">
			<br />
			<div id="checkupdate" class="attention"></div>

			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				
				<table class="form">
					<tr>
						<td>
							<?php echo $txt_option_equal_enableopupdate; ?>
						</td>
						<td>
							<input name="option_equal_enableopupdate" type="checkbox" value="1" <?php echo (!empty($option_equal_enableopupdate) ? 'checked' : ''); ?> />
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $txt_option_equal_ocultpriceop; ?>
						</td>
						<td>
							<input name="option_equal_ocultpriceop" type="checkbox" value="1" <?php echo (!empty($option_equal_ocultpriceop) ? 'checked' : ''); ?> />
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $txt_option_ocultprefix; ?>
						</td>
						<td>
							<input name="option_equal_ocultprefix" type="checkbox" value="1" <?php echo (!empty($option_equal_ocultprefix) ? 'checked' : ''); ?> />
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $txt_option_equal_enableanimation; ?>
						</td>
						<td>
							<input name="option_equal_enableanimation" type="checkbox" value="1" <?php echo (!empty($option_equal_enableanimation) ? 'checked' : ''); ?> />
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $txt_option_equal_alter_price; ?>
						</td>
						<td>
							<input name="option_equal_alter_price" type="text" value="<?php echo $option_equal_alter_price; ?>" size="27" />
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $txt_option_equal_css; ?>
						</td>
						<td>
							<textarea name="option_equal_css" rows="4" cols="30"><?php echo $option_equal_css; ?></textarea>
						</td>
					</tr>
				</table>
			</form>
	</div>
</div>

<script type="text/javascript">
<!--
checking_update_text = "<?php echo $checking_update_text; ?>";
$(function()
{
	$.ajax(
	{
		url: '<?php echo $urlcheckupdate; ?>',
		dataType: 'json',
		beforeSend: function()
		{
			$("#checkupdate").text(checking_update_text);
		},
		success: function(data)
		{
			if(data.update)
			{
				$("#checkupdate").html(data.html);
			}
			else
			{
				$("#checkupdate").html(data.html);
				setTimeout(function()
				{
					$("#checkupdate").hide();
				}, 5000);
			}
		},
		error: function(xhr, ajaxOptions, thrownError)
		{
			setTimeout(function()
			{
				$("#checkupdate").hide();
			}, 5000);
		}
	});		
});
//-->
</script>
<?php echo $footer; ?>