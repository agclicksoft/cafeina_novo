<?php echo $header; ?> 
<style>
.top_tabs{margin-bottom:7px;}
.top_tabs tr td{padding:7px;margin:0;}
.top_tabs tr td a.active{color:#333;text-decoration:none;}
.tab-box tr{border:1px solid #DDDDDD;}
.tab-box tr .left{background:#EFEFEF;font-weight:bold;}
.list tbody td a {text-decoration:none;}
fieldset {width:100px; border:1px solid #ddd;}
fieldset ul {margin:0;padding:0;}
fieldset ul li {list-style-type:none;}
table.form > tbody > tr > td {padding:10px}
table.form > tbody > tr > td:last-child {width:300px;}
.list td {border-right:0 !important;}
.tool-tip {cursor:pointer;}
#tool-tip { display:none; position:absolute; padding:5px; background:#EFEFEF;border:1px solid #ddd;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;z-index:9999;}
#tool-tip #arrow { position: absolute; top: 8px; left: -10px }
.ui-datepicker-calendar tr {border:0 !important;}
.ui-datepicker-calendar tr td {padding:1px !important;}
</style>

<div id="content">
  	<div class="breadcrumb">
    	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
    		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    	<?php } ?>
  	</div>
  	 <?php if ($success) { ?>
  		<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
  	<?php if ($error) { ?>
  		<div class="warning"><?php echo $error; ?></div>
  	<?php } ?>	
	<div class="box">
  		<div class="heading">
    		<h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    		<div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  		</div>
  <div class="content">
  	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
  	<!-- Start of top tabs -->
  	<table cellpadding=0 cellspacing=0 border=0 class='top_tabs'>
  		<tr>
  			<td align='center' valign='middle'><a href='javascript:void(0);' class='active' id='tab_general_settings'><?php echo $tab_general_settings;?></a></td>
  			
  			
  			
  			
  			
  		</tr>
  	</table>
  	<!-- End of top tabs -->

	<!-- Start of General Settings tab -->
	<table class="form tab-box list" id='tab_general_settings_box'>
		<thead>
        	<tr>
          		<td class='left' colspan=3><?php echo $heading_tab_general_settings;?></td>
          	</tr>
      	</thead>	
    	<tr>
      		<td><?php echo $entry_status; ?></td>
      		<td><select name="order_delivery_date_status">
          		<?php if ($order_delivery_date_status) { ?>
          			<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
          			<option value="0"><?php echo $text_disabled; ?></option>
          		<?php } else { ?>
          			<option value="1"><?php echo $text_enabled; ?></option>
          			<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
          		<?php } ?>
        		</select>
        	</td>
        	<td>
        		<img src='view/image/information.png' align='absmiddle' class='tool-tip' tip_data='<?php echo $tool_tip_status;?>'/>
        	</td>
    	</tr>
	</table>
  	<!-- End of General Settings tab -->

	

	

	

	
  		
  	</form>
  </div>
</div>

<script type="text/javascript"><!--
	
	
	$(document).ready(function () {
	
		$('.date-picker').datepicker({dateFormat: 'yy-m-d'})
		
		$('.top_tabs td a ').click(function () {
			$('.top_tabs td a ').removeClass('active');
			$(this).addClass('active');
			$('.tab-box').hide();
			$('#'+$(this).attr('id')+'_box').show();
		});
		$('.tool-tip').mouseleave(function() {
			$('#tool-tip').fadeOut('slow').remove();
		});
		$('.tool-tip').mouseover(function(a) {
			var offset_data = $(this).offset();
			$('body').append('<span id="tool-tip">'+ $(this).attr('tip_data') + "</span>");
			$('#tool-tip').css('top', offset_data.top - 3 + 'px').css('left',offset_data.left + $(this).width()+ 3 + 'px').show();
		});
		
      
				
		
	});
	
	
--></script>
<?php echo $footer; ?>