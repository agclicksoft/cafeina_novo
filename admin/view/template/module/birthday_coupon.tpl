<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
	
	<div id="tabs" class="vtabs">
		<a href="#tab-general"><?php echo $tab_general; ?></a>
		<a href="#tab-coupon"><?php echo $tab_coupon; ?></a>
		<a href="#tab-email-template"><?php echo $tab_email_template; ?></a>
		<a href="#tab-help"><?php echo $tab_help; ?></a>
	</div>
  
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
	  
		<div id="tab-general" class="vtabs-content">
		  <table class="form">
			<tr>
				<td class="left"><span class="required">* </span><?php echo $entry_secret_code; ?></td>
				<td><input type="text" name="birthday_coupon_secret_code" value="<?php echo $birthday_coupon_secret_code; ?>">
				<?php if ($error_secret_code) { ?>
				<span class="error"><?php echo $error_secret_code; ?></span>
				<?php } ?>
				</td>
			</tr>
			<tr>
				<td class="left"><?php echo $entry_allow_update; ?></td>
				<td><select name="birthday_coupon_allow_update">
					<?php if ($birthday_coupon_allow_update) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?> 
				</select></td>
			</tr>
			<tr>
				<td class="left"><?php echo $entry_badge_position; ?></td>
				<td><select name="birthday_coupon_badge_position">
					<?php if ($birthday_coupon_badge_position == "up-right") { ?>
					<option value="up-right" selected="selected"><?php echo $text_up_right; ?></option>
					<?php } else { ?>
					<option value="up-right"><?php echo $text_up_right; ?></option>
					<?php } ?>
					
					<?php if ($birthday_coupon_badge_position == "down-right") { ?>
					<option value="down-right" selected="selected"><?php echo $text_down_right; ?></option>
					<?php } else { ?>
					<option value="down-right"><?php echo $text_down_right; ?></option>
					<?php } ?> 	
					
					<?php if ($birthday_coupon_badge_position == "down-left") { ?>
					<option value="down-left" selected="selected"><?php echo $text_down_left; ?></option>
					<?php } else { ?>
					<option value="down-left"><?php echo $text_down_left; ?></option>
					<?php } ?> 
					
					<?php if ($birthday_coupon_badge_position == "up-left") { ?>
					<option value="up-left" selected="selected"><?php echo $text_up_left; ?></option>
					<?php } else { ?>
					<option value="up-left"><?php echo $text_up_left; ?></option>
					<?php } ?> 
				</select></td>
			</tr>
			<tr>
				<td class="left"><?php echo $entry_badge_color; ?></td>
				<td><input class="choose-color" type="text" name="birthday_coupon_badge_color" value="<?php echo $birthday_coupon_badge_color; ?>"><span class="color-sample" style="background-color: <?php echo $birthday_coupon_badge_color; ?>"></span></td>
			</tr>
			<tr>
				<td class="left"><?php echo $entry_auto_open_times; ?></td>
				<td><input type="text" name="birthday_coupon_auto_open_times" value="<?php echo $birthday_coupon_auto_open_times; ?>"></td>
			</tr>
			<tr>
				<td class="left"><?php echo $entry_admin_report; ?></td>
				<td><select name="birthday_coupon_admin_report">
					<?php if ($birthday_coupon_admin_report) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?> 
				</select></td>
			</tr>			
		  </table>
		  
		  <table id="module" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_layout; ?></td>
              <td class="left"><?php echo $entry_position; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
              <td class="right"><?php echo $entry_sort_order; ?></td>
              <td></td>
            </tr>
          </thead>
          <?php $module_row = 0; ?>
          <?php foreach ($modules as $module) { ?>
          <tbody id="module-row<?php echo $module_row; ?>">
            <tr>
              <td class="left"><select name="birthday_coupon_module[<?php echo $module_row; ?>][layout_id]">
                  <?php foreach ($layouts as $layout) { ?>
                  <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                  <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
              <td class="left"><select name="birthday_coupon_module[<?php echo $module_row; ?>][position]">
                  <?php if ($module['position'] == 'content_top') { ?>
                  <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                  <?php } else { ?>
                  <option value="content_top"><?php echo $text_content_top; ?></option>
                  <?php } ?>
                  <?php if ($module['position'] == 'content_bottom') { ?>
                  <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                  <?php } else { ?>
                  <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                  <?php } ?>
                  <?php if ($module['position'] == 'column_left') { ?>
                  <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                  <?php } else { ?>
                  <option value="column_left"><?php echo $text_column_left; ?></option>
                  <?php } ?>
                  <?php if ($module['position'] == 'column_right') { ?>
                  <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                  <?php } else { ?>
                  <option value="column_right"><?php echo $text_column_right; ?></option>
                  <?php } ?>
                </select></td>
              <td class="left"><select name="birthday_coupon_module[<?php echo $module_row; ?>][status]">
                  <?php if ($module['status']) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
              <td class="right"><input type="text" name="birthday_coupon_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
              <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
            </tr>
          </tbody>
          <?php $module_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="4"></td>
              <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
            </tr>
          </tfoot>
        </table>
		  
		</div> 
	
		<div id="tab-coupon" class="vtabs-content">
			<table class="form">
				<tr>
					<td class="left"><?php echo $entry_type; ?></td>
					<td><select name="birthday_coupon_type">
						<?php if ($birthday_coupon_type == "P") { ?>
						<option value="P" selected="selected"><?php echo $text_percent; ?></option>
						<option value="F"><?php echo $text_fixed; ?></option>
						<?php } else { ?>
						<option value="P"><?php echo $text_percent; ?></option>
						<option value="F" selected="selected"><?php echo $text_fixed; ?></option>
						<?php } ?> 
					</select></td>
				</tr>
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_discount; ?></td>
					<td><input type="text" name="birthday_coupon_discount" value="<?php echo $birthday_coupon_discount; ?>">
					<?php if ($error_discount) { ?>
					<span class="error"><?php echo $error_discount; ?></span>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td class="left"><?php echo $entry_total; ?></td>
					<td><input type="text" name="birthday_coupon_total" value="<?php echo $birthday_coupon_total; ?>"></td>
				</tr>
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_validity_days; ?></td>
					<td><input type="text" name="birthday_coupon_validity_days" value="<?php echo $birthday_coupon_validity_days; ?>">
					<?php if ($error_validity_days) { ?>
					<span class="error"><?php echo $error_validity_days; ?></span>
					<?php } ?>
					</td>
				</tr>
			</table>
		</div> 
	
		<div id="tab-email-template" class="vtabs-content">
			<div id="mail-languages" class="htabs">
				<?php foreach ($languages as $language) { ?>
				<a href="#mail-language-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
				<?php } ?>
			</div>
			
			<?php foreach ($languages as $language) { ?>
			<div id="mail-language-<?php echo $language['language_id']; ?>">
				<table class="form">
					<tr>
						<td class="left"><span class="required">* </span><?php echo $entry_email_subject; ?></td>
						<td><input name="birthday_coupon_email_template[<?php echo $language['language_id']; ?>][subject]" size="100" value="<?php echo isset($birthday_coupon_email_template[$language['language_id']]) ? $birthday_coupon_email_template[$language['language_id']]['subject'] : ''; ?>" />
						<?php if (isset($error_email_subject[$language['language_id']])) { ?>
						<span class="error"><?php echo $error_email_subject[$language['language_id']]; ?></span>
						<?php } ?>
						</td>
					</tr>
					<tr>
						<td class="left"><span class="required">* </span><?php echo $entry_email_message; ?></td>
						<td><textarea name="birthday_coupon_email_template[<?php echo $language['language_id']; ?>][message]" id="birthday_coupon_email_template_<?php echo $language['language_id']; ?>" cols="120" rows="8"><?php echo isset($birthday_coupon_email_template[$language['language_id']]) ? $birthday_coupon_email_template[$language['language_id']]['message'] : ''; ?></textarea>
						<?php if (isset($error_email_message[$language['language_id']])) { ?>
						<span class="error"><?php echo $error_email_message[$language['language_id']]; ?></span>
						<?php } ?>
						</td>
					</tr>
				</table>
			</div>
			<?php } ?>		
		</div>	
		
		<div id="tab-help" class="vtabs-content">
			Changelog and HELP you can find  : <a href="http://oc-extensions.com/Birthday-Coupon" target="blank">HERE</a><br /><br />
			If you need support email us at <strong>support@oc-extensions.com</strong><br /><br /><br />
			
			<u><strong>Become a Premium Member:</strong></u><br /><br />
			With Premium Membership you will can download all our products (past, present and future) starting with the payment date, until the same day and month, a year later. <br />
			Find more on <a href="http://www.oc-extensions.com">www.oc-extensions.com</a>
		</div>
    </form>
  </div>
</div>
<script type="text/javascript"><!--	
$('#tabs a').tabs();
$('#mail-languages a').tabs();

$('input.choose-color').each(function(){
	
	var color_picker_input = $(this);
	
	color_picker_input.ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val('#' + hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);   
		},
		onChange: function (hsb, hex, rgb) {
			color_picker_input.val('#' + hex);
			recolorElements(color_picker_input);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});	
});

$('input.choose-color').bind('change', function(){
	recolorElements($(this));
});

function recolorElements(object) {
	var color = object.val();
	
	object.next().css('background-color', color);
	object.val(color);
}

//--></script>

<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('birthday_coupon_email_template_<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
//--></script>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><select name="birthday_coupon_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="birthday_coupon_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';
	html += '    <td class="left"><select name="birthday_coupon_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="birthday_coupon_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script>
<?php echo $footer; ?>