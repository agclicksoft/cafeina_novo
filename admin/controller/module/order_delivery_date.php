<?php
class ControllerModuleOrderdeliverydate extends Controller {
	private $error = array(); 
	
	public function index() {   
	    $this->load->language('module/order_delivery_date');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			
			$this->model_setting_setting->editSetting('order_delivery_date', $this->request->post);	
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('module/order_delivery_date', 'token=' . $this->session->data['token'], 'SSL'));
		}
		else {
			$this->prepareViewData();
			$this->prepareBreadcrumbs();
			$this->prepareFormData();
			$this->load->model('localisation/language');
			$this->data['languages'] = $this->model_localisation_language->getLanguages();
			$this->template = 'module/order_delivery_date.tpl';
			$this->children = array('common/header', 'common/footer');
			$this->response->setOutput($this->render());
		}
	}

	public function prepareViewData(){
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->session->data['error'])) {
			$this->data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}
			
		$this->data['heading_title'] = $this->language->get('heading_title');
        // tab labels
		$this->data['tab_general_settings'] = $this->language->get('tab_general_settings');
		// tab heading labels
		$this->data['heading_tab_general_settings'] = $this->language->get('heading_tab_general_settings');
		// tab sub heading labels
	    $this->data['text_time_fromat'] = $this->language->get('text_time_fromat');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_required'] = $this->language->get('entry_required');
		//info 
	    // tool tip 
		$this->data['tool_tip_status'] = $this->language->get('tool_tip_status');
		// Button label
		$this->data['button_add_range_hour'] = $this->language->get('button_add_range_hour');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} 
		else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['text'])) {
			$this->data['error_text'] = $this->error['text'];
		} 
		else {
			$this->data['error_text'] = '';
		}
		$this->data['action'] = $this->url->link('module/order_delivery_date', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');	
	}
	
	public function prepareBreadcrumbs(){
		
		$this->data['breadcrumbs'] = array();
   		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);
   		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_module'),
      		'separator' => ' :: '
   		);
   		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('module/order_delivery_date', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
	}
	
	public function prepareFormData(){
		
		//$this->data['delivery_days'] = $this->getDeliveryDays();
		
		if (isset($this->request->post['order_delivery_date_status'])) {
			$this->data['order_delivery_date_status'] = $this->request->post['order_delivery_date_status'];
		} 
		else if ($this->config->get('order_delivery_date_status')) {
			$this->data['order_delivery_date_status'] = $this->config->get('order_delivery_date_status');
		}
		else {
			$this->data['order_delivery_date_status'] = '';
		}
		
	}
	private function validate() {

		if (!$this->user->hasPermission('modify', 'module/order_delivery_date')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>