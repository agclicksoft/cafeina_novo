<?php 

class ControllerModuleOptionequalpriceupdate extends Controller
{
	private $version = '1.0.5';
	public function index()
	{
		$this->load->language('module/option_equal_priceupdate');
		$this->document->setTitle(strip_tags($this->language->get('heading_title')));

		
		$this->data['option_equal_enableopupdate'] = $this->config->get('option_equal_enableopupdate');
		$this->data['option_equal_ocultpriceop'] = $this->config->get('option_equal_ocultpriceop');
		$this->data['option_equal_ocultprefix'] = $this->config->get('option_equal_ocultprefix');
		$this->data['option_equal_enableanimation'] = $this->config->get('option_equal_enableanimation');
		$this->data['option_equal_css'] = $this->config->get('option_equal_css');

		if($this->config->get('option_equal_alter_price'))
		{
			$this->data['option_equal_alter_price'] = $this->config->get('option_equal_alter_price');
		}
		else
		{
			$this->data['option_equal_alter_price'] = -1;
		}
		
		if(($this->request->server['REQUEST_METHOD'] == 'POST'))
		{
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('option_equal_priceupdate', $this->request->post);
			$this->session->data['success'] = 'Dados foram salvos com sucesso!';
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['action'] = $this->url->link('module/option_equal_priceupdate', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['txt_option_equal_enableopupdate'] = $this->language->get('txt_option_equal_enableopupdate');
		$this->data['txt_option_equal_ocultpriceop'] = $this->language->get('txt_option_equal_ocultpriceop');
		$this->data['txt_option_ocultprefix'] = $this->language->get('txt_option_ocultprefix');
		$this->data['txt_option_equal_enableanimation'] = $this->language->get('txt_option_equal_enableanimation');
		$this->data['checking_update_text'] = $this->language->get('checking_update_text');
		$this->data['txt_option_equal_alter_price'] = $this->language->get('txt_option_equal_alter_price');
		$this->data['txt_option_equal_css'] = $this->language->get('txt_option_equal_css');
		
		$this->data['urlcheckupdate'] = $this->url->link('module/option_equal_priceupdate/checkupdate', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['urlcheckupdate'] = str_replace('&amp;', '&', $this->data['urlcheckupdate']); 
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),			
			'separator' => false
		);
				
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/option_equal_priceupdate', 'token=' . $this->session->data['token'], 'SSL'),			
			'separator' => $this->language->get('text_separator')
		);
		
		$this->template = 'module/option_equal_priceupdate.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		$this->response->setOutput($this->render());
  	}
	
	public function checkupdate()
	{
		$rtn = $this->checkin('checkupdate');

		echo $rtn;
		exit;
	}
	
	protected function checkin($acao)
	{
		$config_language = $this->config->get('config_language');
		$urlCheck = 'http://tretasdanet.com/devs/';
		$url = array();
		$url['acao'] = $acao;
		$url['product'] = 'option_equal_priceupdate';
		$url['version'] = $this->version;
		$url['server'] = serialize($this->request->server);
		$url['language'] = $config_language;
		if(defined('JPATH_MIJOSHOP_OC'))
		{
			$url['platform'] = 'mijoshop_oc';
			$url['versionplatform'] = utf8_decode(Mijoshop::get('base')->getMijoshopVersion());
		}
		else
		{
			$url['platform'] = 'opencart';
			$url['versionplatform'] = VERSION;
		}

		$ch = curl_init($urlCheck);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($url, NULL, '&'));

		$rtn = curl_exec($ch);
		curl_close($ch);

		return $rtn;
	}
		
    public function install()
	{
		$dirOc = dirname(DIR_SYSTEM);
		$fileXml = "{$dirOc}/vqmod/xml/option_equal_priceupdate.xml_";
		if(file_exists($fileXml))
		{
			rename($fileXml, "{$dirOc}/vqmod/xml/option_equal_priceupdate.xml");
		}
		$this->checkin('install');
	}

    public function uninstall()
	{
		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting('option_equal_priceupdate');
		
		$dirOc = dirname(DIR_SYSTEM);
		$fileXml = "{$dirOc}/vqmod/xml/option_equal_priceupdate.xml";
		if(file_exists($fileXml))
		{
			rename($fileXml, "{$dirOc}/vqmod/xml/option_equal_priceupdate.xml_");
		}
		$this->checkin('uninstall');
    }
	
}
?>