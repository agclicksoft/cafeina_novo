<?php
class ControllerModuleBirthdayCoupon extends Controller {
	private $error = array();
	private $version = '1.1';

	public function install() {
		$this->load->model('module/birthday_coupon');
		
		$this->model_module_birthday_coupon->createTables();
	}	
	
	public function index() {   
		$this->load->language('module/birthday_coupon');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->document->addScript('view/javascript/jquery/colorpicker/colorpicker.js');
		$this->document->addStyle('view/stylesheet/colorpicker.css');
		$this->document->addStyle('view/stylesheet/birthday_coupon.css');
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('birthday_coupon', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				 
		$this->data['heading_title'] 			  = $this->language->get('heading_title') . ' ' . $this->version;
		
		$this->data['tab_general']    			  = $this->language->get('tab_general');
		$this->data['tab_coupon'] 	              = $this->language->get('tab_coupon');
		$this->data['tab_email_template']    	  = $this->language->get('tab_email_template');
		$this->data['tab_help']       			  = $this->language->get('tab_help');
		
		$this->data['text_enabled']   			  = $this->language->get('text_enabled');
		$this->data['text_disabled']  			  = $this->language->get('text_disabled');
		$this->data['text_yes']       			  = $this->language->get('text_yes');
		$this->data['text_no']        			  = $this->language->get('text_no');
		$this->data['text_content_top']           = $this->language->get('text_content_top');
		$this->data['text_content_bottom']        = $this->language->get('text_content_bottom');		
		$this->data['text_column_left']           = $this->language->get('text_column_left');
		$this->data['text_column_right']          = $this->language->get('text_column_right');
		
		$this->data['text_up_right']              = $this->language->get('text_up_right');
		$this->data['text_down_right']            = $this->language->get('text_down_right');
		$this->data['text_down_left']             = $this->language->get('text_down_left');
		$this->data['text_up_left']               = $this->language->get('text_up_left');
		
		$this->data['text_percent']        		  = $this->language->get('text_percent');
		$this->data['text_fixed']        		  = $this->language->get('text_fixed');
		
		$this->data['entry_layout']               = $this->language->get('entry_layout');
		$this->data['entry_position']             = $this->language->get('entry_position');
		$this->data['entry_status']               = $this->language->get('entry_status');
		$this->data['entry_sort_order']           = $this->language->get('entry_sort_order');
		
		$this->data['entry_secret_code']     	  = $this->language->get('entry_secret_code');
		$this->data['entry_allow_update']     	  = $this->language->get('entry_allow_update');
		$this->data['entry_badge_position']       = $this->language->get('entry_badge_position');
		$this->data['entry_badge_color']     	  = $this->language->get('entry_badge_color');
		$this->data['entry_auto_open_times']      = $this->language->get('entry_auto_open_times');
		$this->data['entry_admin_report'] 	      = $this->language->get('entry_admin_report');
		
		$this->data['entry_type'] 	  			  = $this->language->get('entry_type');
		$this->data['entry_discount']   		  = $this->language->get('entry_discount');
		$this->data['entry_total'] 	  			  = $this->language->get('entry_total');
		$this->data['entry_validity_days'] 	      = $this->language->get('entry_validity_days');
		
		$this->data['entry_email_subject'] 	      = $this->language->get('entry_email_subject');
		$this->data['entry_email_message'] 	      = $this->language->get('entry_email_message');
		
		$this->data['button_save']                = $this->language->get('button_save');
		$this->data['button_cancel']              = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['secret_code'])) {
			$this->data['error_secret_code'] = $this->error['secret_code'];
		} else {
			$this->data['error_secret_code'] = '';
		}	
		
		if (isset($this->error['discount'])) {
			$this->data['error_discount'] = $this->error['discount'];
		} else {
			$this->data['error_discount'] = '';
		}
		
		if (isset($this->error['validity_days'])) {
			$this->data['error_validity_days'] = $this->error['validity_days'];
		} else {
			$this->data['error_validity_days'] = '';
		}

		if (isset($this->error['email_subject'])) {
			$this->data['error_email_subject'] = $this->error['email_subject'];
		} else {
			$this->data['error_email_subject'] = array();
		}		
		
		if (isset($this->error['email_message'])) {
			$this->data['error_email_message'] = $this->error['email_message'];
		} else {
			$this->data['error_email_message'] = array();
		}	
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/birthday_coupon', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/birthday_coupon', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['birthday_coupon_secret_code'])){
			$this->data['birthday_coupon_secret_code'] = $this->request->post['birthday_coupon_secret_code'];
		} elseif ( $this->config->get('birthday_coupon_secret_code')){
			$this->data['birthday_coupon_secret_code'] = $this->config->get('birthday_coupon_secret_code');
		} else {
			$this->data['birthday_coupon_secret_code'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_allow_update'])){
			$this->data['birthday_coupon_allow_update'] = $this->request->post['birthday_coupon_allow_update'];
		} elseif ( $this->config->get('birthday_coupon_allow_update')){
			$this->data['birthday_coupon_allow_update'] = $this->config->get('birthday_coupon_allow_update');
		} else {
			$this->data['birthday_coupon_allow_update'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_badge_position'])){
			$this->data['birthday_coupon_badge_position'] = $this->request->post['birthday_coupon_badge_position'];
		} elseif ( $this->config->get('birthday_coupon_badge_position')){
			$this->data['birthday_coupon_badge_position'] = $this->config->get('birthday_coupon_badge_position');
		} else {
			$this->data['birthday_coupon_badge_position'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_badge_color'])){
			$this->data['birthday_coupon_badge_color'] = $this->request->post['birthday_coupon_badge_color'];
		} elseif ( $this->config->get('birthday_coupon_badge_color')){
			$this->data['birthday_coupon_badge_color'] = $this->config->get('birthday_coupon_badge_color');
		} else {
			$this->data['birthday_coupon_badge_color'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_auto_open_times'])){
			$this->data['birthday_coupon_auto_open_times'] = $this->request->post['birthday_coupon_auto_open_times'];
		} elseif ( $this->config->get('birthday_coupon_auto_open_times')){
			$this->data['birthday_coupon_auto_open_times'] = $this->config->get('birthday_coupon_auto_open_times');
		} else {
			$this->data['birthday_coupon_auto_open_times'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_admin_report'])){
			$this->data['birthday_coupon_admin_report'] = $this->request->post['birthday_coupon_admin_report'];
		} elseif ( $this->config->get('birthday_coupon_admin_report')){
			$this->data['birthday_coupon_admin_report'] = $this->config->get('birthday_coupon_admin_report');
		} else {
			$this->data['birthday_coupon_admin_report'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_type'])){
			$this->data['birthday_coupon_type'] = $this->request->post['birthday_coupon_type'];
		} elseif ( $this->config->get('birthday_coupon_type')){
			$this->data['birthday_coupon_type'] = $this->config->get('birthday_coupon_type');
		} else {
			$this->data['birthday_coupon_type'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_discount'])){
			$this->data['birthday_coupon_discount'] = $this->request->post['birthday_coupon_discount'];
		} elseif ( $this->config->get('birthday_coupon_discount')){
			$this->data['birthday_coupon_discount'] = $this->config->get('birthday_coupon_discount');
		} else {
			$this->data['birthday_coupon_discount'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_total'])){
			$this->data['birthday_coupon_total'] = $this->request->post['birthday_coupon_total'];
		} elseif ( $this->config->get('birthday_coupon_total')){
			$this->data['birthday_coupon_total'] = $this->config->get('birthday_coupon_total');
		} else {
			$this->data['birthday_coupon_total'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_validity_days'])){ 
			$this->data['birthday_coupon_validity_days'] = $this->request->post['birthday_coupon_validity_days'];
		} elseif ( $this->config->get('birthday_coupon_validity_days')){
			$this->data['birthday_coupon_validity_days'] = $this->config->get('birthday_coupon_validity_days');
		} else {
			$this->data['birthday_coupon_validity_days'] = '';
		}
		
		if (isset($this->request->post['birthday_coupon_email_template'])){
			$this->data['birthday_coupon_email_template'] = $this->request->post['birthday_coupon_email_template'];
		} elseif ( $this->config->get('birthday_coupon_email_template')){
			$this->data['birthday_coupon_email_template'] = $this->config->get('birthday_coupon_email_template');
		} else {
			$this->data['birthday_coupon_email_template'] = '';
		}
		
		$this->data['modules'] = array();
		
		if (isset($this->request->post['birthday_coupon_module'])) {
			$this->data['modules'] = $this->request->post['birthday_coupon_module'];
		} elseif ($this->config->get('birthday_coupon_module')) { 
			$this->data['modules'] = $this->config->get('birthday_coupon_module');
		}
		
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		
		$this->data['token'] = $this->session->data['token'];
						 
		$this->template = 'module/birthday_coupon.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
	
		if (!$this->user->hasPermission('modify', 'module/birthday_coupon')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$dinamic_strlen = 'utf8_strlen';
		 
		if ( !function_exists('utf8_strlen') ) {
			$dinamic_strlen = 'strlen';
		}
		
		if ($dinamic_strlen($this->request->post['birthday_coupon_secret_code']) == 0){
			$this->error['secret_code'] = $this->language->get('error_secret_code');
		}
		
		if ($dinamic_strlen($this->request->post['birthday_coupon_discount']) == 0){
			$this->error['discount'] = $this->language->get('error_discount');
		}
		
		if ($dinamic_strlen($this->request->post['birthday_coupon_validity_days']) == 0){
			$this->error['validity_days'] = $this->language->get('error_validity_days');
		}
		
		if (isset($this->request->post['birthday_coupon_email_template'])) {
			foreach ($this->request->post['birthday_coupon_email_template'] as $language_id => $value) {
				if ($dinamic_strlen($value['subject']) < 1) {
					$this->error['email_subject'][$language_id] = $this->language->get('error_email_subject');
				}
				
				if ($dinamic_strlen($value['message']) < 1) {
					$this->error['email_message'][$language_id] = $this->language->get('error_email_message');
				}			
			}
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>