<?php 
	session_start(); 
	if($_SESSION["logado"] == 0){
		header('Location: index.php');					
	}	
	

?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>

		<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1" >

		<title> Delivery </title>
		<link type="text/css" rel="stylesheet" media="screen" href="css/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" media="screen" href="css/admin.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css?v=2.1.5" media="screen" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.fancybox.js?v=2.1.5"></script>
		
		<script>
			$(document).ready(function() {

				$('.fancybox').fancybox();		
			})

			setInterval(atualizar, 10000);
			var audioElement = document.createElement('audio');
			function atualizar(){
				 
				audioElement.setAttribute('src', 'music/Alarm-Clock Sound!!! - YouTube.mp3');
				//audioElement.setAttribute('autoplay', 'autoplay');
				
				$.ajax({
					type: "json",
					url: "verificar_delivery.php",
					success: function(data){
						if(data > 0){
							vd =$("#verificar_delivery").val();
							if(vd == "0"){
								$("#verificar_delivery").val("1");

								playD();
								backgroundD();
								$("#table_delivery").load("delivery_list.php #table_delivery");
								$(".nenhum_delivery").css("display", "none");
								//audioElement.setAttribute('autoplay', 'autoplay');
								//alertD();
								//updateDisplay();
							}
							
						}
					}

				});
			}
			
			function pauseD(){
				//audioElement.pause();	
				updateDisplay();		
			}
			function playD(){
				setInterval(function(){audioElement.play()}, 2000);
				
			}

			function backgroundD(){
				//$("body").css("background-color", "red");
				setInterval(function(){
					if($("#verificar_delivery").val() == "1"){
						$("body").css("background-color", "red");
						$("#verificar_delivery").val("0");					
					}
					else{
						$("body").css("background-color", "#fff");
						$("#verificar_delivery").val("1");					
					}
				},2000);
		}			
			
			function alertD(){
				alert("Novo delivery");	
			};
			function updateDisplay(){
				$.ajax({
					type: "POST",
					url: "updateDisplay.php",
					success:function(data){
						$("#verificar_delivery").val("0");
						location.reload();
					}
				});
			}								

			function ocultarPedido(id){
				if (confirm('Deseja ocultar pedido?')) {
					$("#pedido_"+id).css("display","none");
					$.ajax({
						type: "POST",
						url: "update.php",
						data: { id: id }

					});
				} else {
					//alert('O registro não foi deletado');
				}
				//alert(id);
	
			}
		</script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<div class="nav-collapse">
						<ul class="nav" style="float: right;">
						<li><a rel="nofollow" data-method="delete" data-confirm="Tem certeza que deseja sair?" href="sign_out.php">Sair</a></li>
				    </ul></div>
				    
			</div><!-- /navbar-inner -->
		</div>		
		</div>
		<div class="container">
			<div id="tudo" style="min-height:700px;">
				<div class="row">
					<div class="span12 sessao">
						<input id="verificar_delivery" type="hidden" value="0"/>
						<h2>Notifica&ccedil;&atilde;o de delivery</h2>
						<p>
							<div class="btn btn-primary btn-small" onclick="pauseD()">Parar Alerta</div>
						</p>
					</div>
					<?php
						include("conexao.php");	
						$sql = "SELECT od.entity_id, od.increment_id, od.base_total_due, od.customer_firstname, customer_lastname FROM";
						$sql .= " mag_sales_flat_order as od";
						$sql .=	" Inner join mag_sales_flat_order_item as item on item.order_id = od.entity_id";
						$sql .=	" Inner Join mag_catalog_category_product as cp on cp.product_id =item.product_id";
						$sql .= " where item.parent_item_id is null and";
						$sql .= " od.status = 'complete'";
						$sql .= " and od.display_active = true";
						$sql .= " and cp.category_id = 60";
						$sql .= " group by entity_id";
	
					    	$sql .= " order by increment_id desc;";

						$qry = mysql_query($sql);
					 	$table = '<table id="table_delivery" class="table"><thead><tr>';

						$table.= '<th>Pedido</th>';
						$table.= '<th>Cliente</th>';
						//$table.= '<th>Entrega Para</th>';
						$table.= '<th>Valor Pago</th>';
						$table.= '<th>Ocultar pedido</th>';
						$table.= '</tr></thead>';
	
						//Montando o corpo da tabela
						$table .= '<tbody>';
						while($r = mysql_fetch_assoc($qry)){
							$number = $r["base_total_due"];
							$nombre_format_francais = number_format($number, 2, ',', ' ');
							$table .= '<tr id="pedido_'.$r["entity_id"].'">';
							$table .= '<td><a class="fancybox fancybox.iframe" href="pedido.php?pedido='.$r["increment_id"].'">'.$r["increment_id"].'</a></td>';
							$table .= '<td>'.$r["customer_firstname"].' '.$r["customer_lastname"].'</td>';
							//$table .= '<td> </td>';
							$table .= '<td>R$'.$nombre_format_francais.'</td>';
							$table .= '<td><span class="ocultar" onclick="ocultarPedido('.$r["entity_id"].');">ocultar</span></td>';
							$table .= '</tr>';
						}
						 
						//Finalizando a tabela
						$table .= '</tbody></table>';
						 
						//Imprimindo a tabela
						echo $table;
	
						if (mysql_num_rows($qry) == 0) {
						    echo "<div class='nenhum_delivery'>Nenhum delivery</div>";
						    exit;
						}
					?>
				</div>
			</div>
		</div>
		<?php include("footer.php"); ?>
	</body>
</html>


