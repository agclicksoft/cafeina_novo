<?php   
class ControllerCronBirthdayCoupon extends Controller {
	public function index() {
		$this->load->model('module/birthday_coupon');		
		
		if (!isset($this->request->get['secret_code'])){
			echo "You forgot secret code";
			exit;
		}
		
		if ($this->request->get['secret_code'] != $this->config->get('birthday_coupon_secret_code')){
			echo "Access Denied: Wrong secret code";
			exit;
		}
		
		$customers = $this->model_module_birthday_coupon->getCustomersCelebratedToday();
		
		if ($customers){
			foreach($customers as $customer){
				$this->sendHappyBirthdayEmail($customer); 
				echo "Sending to " . $customer['firstname'] . " " . $customer['lastname'] . " (" . $customer['email'] . ") <br />";	
			}	
		} else {
			echo "No customer celebrate his birthday today";
		}

		if ($this->config->get('birthday_coupon_admin_report') && $customers) {
			$this->sendAdminReport();
		}		
	}

	private function sendHappyBirthdayEmail($customer){
		$this->load->model('module/birthday_coupon');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		
		$coupon_code = $this->generateCode($customer['customer_id']);
		
		$find = array(
			'{firstname}',
			'{lastname}',
			'{coupon_code}',
			'{discount}',
			'{total_amount}',
			'{validity_days}',
			'{store_name}',
		);
		
		$replace = array(
			'firstname'        		   => $customer['firstname'],
			'lastname'         		   => $customer['lastname'],
			'coupon_code'              => $coupon_code,
			'discount'                 => ($this->config->get('birthday_coupon_type') == 'F') ? $this->currency->format($this->config->get('birthday_coupon_discount')) : $this->config->get('birthday_coupon_discount') . '%',
			'total_amount'			   => $this->currency->format($this->config->get('birthday_coupon_total')),
			'validity_days'            => $this->config->get('birthday_coupon_validity_days'),
			'store_name'               => $this->config->get('config_name')	
		);
		
		$email_template_languages = $this->config->get('birthday_coupon_email_template');
		$email_template = $email_template_languages[$this->config->get('config_language_id')];
		
		$subject = str_replace($find, $replace, html_entity_decode($email_template['subject'], ENT_QUOTES, 'UTF-8'));
		$message = str_replace($find, $replace, html_entity_decode($email_template['message'], ENT_QUOTES, 'UTF-8'));
		
		$template = new Template();
		
		$template->data['logo'] = $server . 'image/' . $this->config->get('config_logo');		
		$template->data['store_name'] = $this->config->get('config_name');	
		$template->data['store_url'] = $this->config->get('config_use_ssl') ? $this->config->get('config_ssl') : $this->config->get('config_url');
		$template->data['message'] = $message;
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/birthday_coupon.tpl')) {
			$html = $template->fetch($this->config->get('config_template') . '/template/mail/birthday_coupon.tpl');
		} else {
			$html = $template->fetch('default/template/mail/birthday_coupon.tpl');
		}
		
		$mail = new Mail(); 
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port 	= $this->config->get('config_smtp_port');
		$mail->timeout  = $this->config->get('config_smtp_timeout');	
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($subject);
		$mail->setTo($customer['email']);
		$mail->setHtml($html);
		$mail->send();
		
		$this->model_module_birthday_coupon->addCoupon($customer, $coupon_code);
		$this->model_module_birthday_coupon->addHistory($customer['customer_id'], $coupon_code);
	}
	
	private function sendAdminReport(){
		$this->language->load('mail/birthday_coupon');
		
		$this->load->model('module/birthday_coupon');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		
		$customers = $this->model_module_birthday_coupon->getHappyBirthdayCouponsSentToday();
		
		$template = new Template();

		$template->data['logo'] = $server . 'image/' . $this->config->get('config_logo');		
		$template->data['store_name'] = $this->config->get('config_name');	
		$template->data['store_url'] = $this->config->get('config_use_ssl') ? $this->config->get('config_ssl') : $this->config->get('config_url');
		
		$template->data['title'] = $this->language->get('text_subject');
		$template->data['text_hi'] = $this->language->get('text_hi');
		$template->data['text_info'] = $this->language->get('text_info');
		
		$template->data['column_customer'] = $this->language->get('column_customer');
		$template->data['column_code'] = $this->language->get('column_code');
		$template->data['column_type'] = $this->language->get('column_type');
		$template->data['column_discount'] = $this->language->get('column_discount');
		$template->data['column_total'] = $this->language->get('column_total');
		$template->data['column_validity'] = $this->language->get('column_validity');
		
		$template->data['customers'] = array();
		
		foreach($customers as $customer) {
			$template->data['customers'][] = array(
				'customer'   => $customer['customer_name'],
				'code'       => $customer['code'],
				'type'       => ($customer['type'] == "P")? $this->language->get('text_percent') : $this->language->get('text_fixed'),
				'discount'   => ($customer['type'] == "P")? $customer['discount'] : $this->currency->format($customer['discount']),
				'total'      => ((float)$customer['total'] > 0 ) ? $this->currency->format($customer['total']) : '-',
				'validity'   => date($this->language->get('date_format_short'), strtotime($customer['validity']))
			);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/birthday_coupon_report.tpl')) {
			$html = $template->fetch($this->config->get('config_template') . '/template/mail/birthday_coupon_report.tpl');
		} else {
			$html = $template->fetch('default/template/mail/birthday_coupon_report.tpl');
		}
		
		$mail = new Mail(); 
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port 	= $this->config->get('config_smtp_port');
		$mail->timeout  = $this->config->get('config_smtp_timeout');	
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($this->language->get('text_subject'));
		$mail->setTo($this->config->get('config_email'));
		$mail->setHtml($html);
		$mail->send();
	}
	
	private function generateCode($customer_id){
		$code = 'HB' . $customer_id;
		$temp_len = strlen($code);
		$diff = 10 - $temp_len;
		$ucode = md5(time());
		$code .= substr($ucode,0, $diff);
		
		return strtoupper($code);
	}
}
?>