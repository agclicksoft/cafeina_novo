<?php
class ControllerModuleBirthdayCoupon extends Controller {
	protected function index($setting) {
		$this->language->load('module/birthday_coupon');
		
		$this->load->model('module/birthday_coupon');
		
		$this->document->addScript('catalog/view/javascript/jquery/jquery.magnific-popup.min.js'); 
		
		if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/birthday_coupon.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/birthday_coupon.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/birthday_coupon.css');
		}
		
		$this->data['text_popup_message'] = $this->language->get('text_popup_message');
		$this->data['text_dont_show_again'] = $this->language->get('text_dont_show_again');
		
		$this->data['entry_year'] = $this->language->get('entry_year');
		$this->data['entry_month'] = $this->language->get('entry_month');
		$this->data['entry_day'] = $this->language->get('entry_day');
		
		$this->data['button_no_thanks'] = $this->language->get('button_no_thanks');
		$this->data['button_set_birthday'] = $this->language->get('button_set_birthday');
		
		$this->data['badge_position'] = $this->config->get('birthday_coupon_badge_position');
		$this->data['badge_color'] = $this->config->get('birthday_coupon_badge_color');
		
		$this->data['current_year'] = date('Y');
		
		$this->data['birth_year'] = '';
		$this->data['birth_month'] = '';
		$this->data['birth_day'] = '';
		
		$show = false;
		$auto_open = true;
		
		if ($this->customer->isLogged() && (!$this->model_module_birthday_coupon->isBirthdayAdded() || $this->config->get('birthday_coupon_allow_update'))) {
			$show = true;
			
			if ($this->model_module_birthday_coupon->isBirthdayAdded()) {
				$this->data['birth_year'] = $this->model_module_birthday_coupon->getBirthYear();
				$this->data['birth_month'] = $this->model_module_birthday_coupon->getBirthMonth();
				$this->data['birth_day'] = $this->model_module_birthday_coupon->getBirthDay();
			}
			
			if (!isset($this->session->data['birthday_coupon_auto_open_times'])) {
				$this->session->data['birthday_coupon_auto_open_times'] = 0;
			} 
			
			if (!$this->config->get('birthday_coupon_auto_open_times') || $this->session->data['birthday_coupon_auto_open_times'] >= $this->config->get('birthday_coupon_auto_open_times') || isset($this->session->data['birthday_coupon_dontshowpopup']) ) {
				$auto_open = false;
			}			
		}
		
		$this->data['auto_open'] = $auto_open;
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/birthday_coupon.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/birthday_coupon.tpl';
		} else {
			$this->template = 'default/template/module/birthday_coupon.tpl';
		}
		
		if ($show) {
			$this->render();
		}	
	}
	
	public function dontshowpopup() {
		$this->session->data['birthday_coupon_dontshowpopup'] = 1;
	}
	
	public function addautoopencount() {
		++$this->session->data['birthday_coupon_auto_open_times'];
	}
	
	public function setbirthdate() {
		$this->language->load('module/birthday_coupon');
		$this->load->model('module/birthday_coupon');
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
		
			if (!$json) {
				if (checkdate($this->request->post['birth_month'], $this->request->post['birth_day'], $this->request->post['birth_year']) == false) {
					$json['error']['warning'] = $this->language->get('error_incorrect_birthdate');
				}
			}

			if (!$json) {
				if (!$this->model_module_birthday_coupon->isBirthdayAdded()) {
					$this->model_module_birthday_coupon->addBirthday($this->request->post);
					$json['success'] = $this->language->get('text_success_birthday_add');
				} else {
					$this->model_module_birthday_coupon->editBirthday($this->request->post);
					$json['success'] = $this->language->get('text_success_birthday_update');
				}
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
}
?>