<?php 
class ControllerCheckoutCart extends Controller {
	private $error = array();
	
	public function index() {
		$this->language->load('checkout/cart');
        
		if (!isset($this->session->data['vouchers'])) {
			$this->session->data['vouchers'] = array();
		}
		
		// Update
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->cart->update($key, $value);
			}
			
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']); 
			unset($this->session->data['reward']);
			
			$this->redirect($this->url->link('checkout/cart'));  			
		}
       	
		// Remove
		if (isset($this->request->get['remove'])) {
			$this->cart->remove($this->request->get['remove']);
			
			unset($this->session->data['vouchers'][$this->request->get['remove']]);
			
			$this->session->data['success'] = $this->language->get('text_remove');
		
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']); 
			unset($this->session->data['reward']);  
								
			$this->redirect($this->url->link('checkout/cart'));
		}
			
		// Coupon    
		if (isset($this->request->post['coupon']) && $this->validateCoupon()) { 
			$this->session->data['coupon'] = $this->request->post['coupon'];
				
			$this->session->data['success'] = $this->language->get('text_coupon');
			
			$this->redirect($this->url->link('checkout/cart'));
		}
		
		// Voucher
		if (isset($this->request->post['voucher']) && $this->validateVoucher()) { 
			$this->session->data['voucher'] = $this->request->post['voucher'];
				
			$this->session->data['success'] = $this->language->get('text_voucher');
				
			$this->redirect($this->url->link('checkout/cart'));
		}

		// Reward
		if (isset($this->request->post['reward']) && $this->validateReward()) { 
			$this->session->data['reward'] = abs($this->request->post['reward']);
				
			$this->session->data['success'] = $this->language->get('text_reward');
				
			$this->redirect($this->url->link('checkout/cart'));
		}
		

		// Shipping
		if (isset($this->request->post['shipping_method']) && $this->validateShipping()) {
			$shipping = explode('.', $this->request->post['shipping_method']);
			
			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
			
			$this->session->data['success'] = $this->language->get('text_shipping');
			
			$this->redirect($this->url->link('checkout/cart'));
		}
		
		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
			
      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('common/home'),
        	'text'      => $this->language->get('text_home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('checkout/cart'),
        	'text'      => $this->language->get('heading_title'),
        	'separator' => $this->language->get('text_separator')
      	);
			
    	if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
			$points = $this->customer->getRewardPoints();
			
			$points_total = 0;
			
			foreach ($this->cart->getProducts() as $product) {
				if ($product['points']) {
					$points_total += $product['points'];
				}
			}		
				
      		$this->data['heading_title'] = $this->language->get('heading_title');
			
			$this->data['text_next'] = $this->language->get('text_next');
			$this->data['text_next_choice'] = $this->language->get('text_next_choice');
     		$this->data['text_use_coupon'] = $this->language->get('text_use_coupon');
			$this->data['text_use_voucher'] = $this->language->get('text_use_voucher');
			$this->data['text_use_reward'] = sprintf($this->language->get('text_use_reward'), $points);
			$this->data['text_shipping_estimate'] = $this->language->get('text_shipping_estimate');
			$this->data['text_shipping_detail'] = $this->language->get('text_shipping_detail');
			$this->data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$this->data['text_select'] = $this->language->get('text_select');
			$this->data['text_none'] = $this->language->get('text_none');
			$this->data['text_until_cancelled'] = $this->language->get('text_until_cancelled');
			$this->data['text_freq_day'] = $this->language->get('text_freq_day');
			$this->data['text_freq_week'] = $this->language->get('text_freq_week');
			$this->data['text_freq_month'] = $this->language->get('text_freq_month');
			$this->data['text_freq_bi_month'] = $this->language->get('text_freq_bi_month');
			$this->data['text_freq_year'] = $this->language->get('text_freq_year');

			$this->data['column_image'] = $this->language->get('column_image');
      		$this->data['column_name'] = $this->language->get('column_name');
      		$this->data['column_model'] = $this->language->get('column_model');
      		$this->data['column_quantity'] = $this->language->get('column_quantity');
			$this->data['column_price'] = $this->language->get('column_price');
      		$this->data['column_total'] = $this->language->get('column_total');
			
			$this->data['entry_coupon'] = $this->language->get('entry_coupon');
			$this->data['entry_voucher'] = $this->language->get('entry_voucher');
			$this->data['entry_reward'] = sprintf($this->language->get('entry_reward'), $points_total);
			$this->data['entry_country'] = $this->language->get('entry_country');
			$this->data['entry_zone'] = $this->language->get('entry_zone');
			$this->data['entry_postcode'] = $this->language->get('entry_postcode');
						
			$this->data['button_update'] = $this->language->get('button_update');
			$this->data['button_remove'] = $this->language->get('button_remove');
			$this->data['button_coupon'] = $this->language->get('button_coupon');
			$this->data['button_voucher'] = $this->language->get('button_voucher');
			$this->data['button_reward'] = $this->language->get('button_reward');
			$this->data['button_quote'] = $this->language->get('button_quote');
			$this->data['button_shipping'] = $this->language->get('button_shipping');			
      		$this->data['button_shopping'] = $this->language->get('button_shopping');
      		$this->data['button_checkout'] = $this->language->get('button_checkout');

      		$this->data['text_trial'] = $this->language->get('text_trial');
      		$this->data['text_recurring'] = $this->language->get('text_recurring');
      		$this->data['text_length'] = $this->language->get('text_length');
      		$this->data['text_recurring_item'] = $this->language->get('text_recurring_item');
      		$this->data['text_payment_profile'] = $this->language->get('text_payment_profile');

			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
			} elseif (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
      			$this->data['error_warning'] = $this->language->get('error_stock');		
			} else {
				$this->data['error_warning'] = '';
			}
			
			if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
				$this->data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$this->data['attention'] = '';
			}
						
			if (isset($this->session->data['success'])) {
				$this->data['success'] = $this->session->data['success'];
			
				unset($this->session->data['success']);
			} else {
				$this->data['success'] = '';
			}
			
			// Validate minimum quantity requirments.			
			$products = $this->cart->getProducts();

			//Verificar se o produto pertence a categoria de delivery 
			$this->load->model('catalog/product');
			$count = 0;
			foreach ($products as $product) {
				if($count == 0){
					$category_id_product_cart = $this->model_catalog_product->getCategories($product['product_id']);			
					$this->data['category_id_product_cart'] = $this->model_catalog_product->getCategoryIdByCategoryId($category_id_product_cart);
					$count .= 1;
				}
			}
				
			$this->data['action'] = $this->url->link('checkout/cart');   
						
			if ($this->config->get('config_cart_weight')) {
				$this->data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
			} else {
				$this->data['weight'] = '';
			}
						 
			$this->load->model('tool/image');

            $this->data['products'] = array();

            $products = $this->cart->getProducts();

            $product_quantity = 0;

            foreach ($products as $product) {
                $product_total = 0;
                $product_quantity += $product['quantity'];

                foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $product['product_id']) {
                        $product_total += $product_2['quantity'];
                    }
                }

                if ($product['minimum'] > $product_total) {
                    $this->data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
                }

                if ($product['image']) {
                    $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                } else {
                    $image = '';
                }

                $option_data = array();

                foreach ($product['option'] as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['option_value'];
                    } else {
                        $filename = $this->encryption->decrypt($option['option_value']);

                        $value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
                    }

                    $option_data[] = array(
                        'name'  => $option['name'],
                        'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                    );
                }

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
                } else {
                    $total = false;
                }
                
                $profile_description = '';
                
                if ($product['recurring']) {
                    $frequencies = array(
                        'day' => $this->language->get('text_day'),
                        'week' => $this->language->get('text_week'),
                        'semi_month' => $this->language->get('text_semi_month'),
                        'month' => $this->language->get('text_month'),
                        'year' => $this->language->get('text_year'),
                    );

                    if ($product['recurring_trial']) {
                        $recurring_price = $this->currency->format($this->tax->calculate($product['recurring_trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')));
                        $profile_description = sprintf($this->language->get('text_trial_description'), $recurring_price, $product['recurring_trial_cycle'], $frequencies[$product['recurring_trial_frequency']], $product['recurring_trial_duration']) . ' ';
                    }

                    $recurring_price = $this->currency->format($this->tax->calculate($product['recurring_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')));

                    if ($product['recurring_duration']) {
                        $profile_description .= sprintf($this->language->get('text_payment_description'), $recurring_price, $product['recurring_cycle'], $frequencies[$product['recurring_frequency']], $product['recurring_duration']);
                    } else {
                        $profile_description .= sprintf($this->language->get('text_payment_until_canceled_description'), $recurring_price, $product['recurring_cycle'], $frequencies[$product['recurring_frequency']], $product['recurring_duration']);
                    }
                }

                /*echo "<pre>";
                print_r($product);*/

                $this->data['products'][] = array(
                    'key'                 => $product['key'],
                    'thumb'               => $image,
                    'name'                => $product['name'],
                    'model'               => $product['model'],
                    'option'              => $option_data,
                    'quantity'            => $product['quantity'],
                    'stock'               => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
                    'reward'              => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                    'price'               => $price,
                    'total'               => $total,
                    'href'                => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    'remove'              => $this->url->link('checkout/cart', 'remove=' . $product['key']),
                    'recurring'           => $product['recurring'],
                    'profile_name'        => $product['profile_name'],
                    'profile_description' => $profile_description,
                );
            }

            //total de produtos
            $this->data['product_quantity'] = $product_quantity;

            $this->data['products_recurring'] = array();
            
			// Gift Voucher
			$this->data['vouchers'] = array();
			
			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$this->data['vouchers'][] = array(
						'key'         => $key,
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount']),
						'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)   
					);
				}
			}

			if (isset($this->request->post['next'])) {
				$this->data['next'] = $this->request->post['next'];
			} else {
				$this->data['next'] = '';
			}
						 
			$this->data['coupon_status'] = $this->config->get('coupon_status');
			
			if (isset($this->request->post['coupon'])) {
				$this->data['coupon'] = $this->request->post['coupon'];			
			} elseif (isset($this->session->data['coupon'])) {
				$this->data['coupon'] = $this->session->data['coupon'];
			} else {
				$this->data['coupon'] = '';
			}
			
			$this->data['voucher_status'] = $this->config->get('voucher_status');
			
			if (isset($this->request->post['voucher'])) {
				$this->data['voucher'] = $this->request->post['voucher'];				
			} elseif (isset($this->session->data['voucher'])) {
				$this->data['voucher'] = $this->session->data['voucher'];
			} else {
				$this->data['voucher'] = '';
			}
			
			$this->data['reward_status'] = ($points && $points_total && $this->config->get('reward_status'));
			
			if (isset($this->request->post['reward'])) {
				$this->data['reward'] = $this->request->post['reward'];				
			} elseif (isset($this->session->data['reward'])) {
				$this->data['reward'] = $this->session->data['reward'];
			} else {
				$this->data['reward'] = '';
			}

			$this->data['shipping_status'] = $this->config->get('shipping_status') && $this->config->get('shipping_estimator') && $this->cart->hasShipping();	
								
			if (isset($this->request->post['country_id'])) {
				$this->data['country_id'] = $this->request->post['country_id'];				
			} elseif (isset($this->session->data['shipping_country_id'])) {
				$this->data['country_id'] = $this->session->data['shipping_country_id'];			  	
			} else {
				$this->data['country_id'] = $this->config->get('config_country_id');
			}
				
			$this->load->model('localisation/country');
			
			$this->data['countries'] = $this->model_localisation_country->getCountries();
						
			if (isset($this->request->post['zone_id'])) {
				$this->data['zone_id'] = $this->request->post['zone_id'];				
			} elseif (isset($this->session->data['shipping_zone_id'])) {
				$this->data['zone_id'] = $this->session->data['shipping_zone_id'];			
			} else {
				$this->data['zone_id'] = '';
			}
			
			if (isset($this->request->post['postcode'])) {
				$this->data['postcode'] = $this->request->post['postcode'];				
			} elseif (isset($this->session->data['shipping_postcode'])) {
				$this->data['postcode'] = $this->session->data['shipping_postcode'];					
			} else {
				$this->data['postcode'] = '';
			}
			
			if (isset($this->request->post['shipping_method'])) {
				$this->data['shipping_method'] = $this->request->post['shipping_method'];				
			} elseif (isset($this->session->data['shipping_method'])) {
				$this->data['shipping_method'] = $this->session->data['shipping_method']['code']; 
			} else {
				$this->data['shipping_method'] = '';
			}
						
			// Totals
			$this->load->model('setting/extension');
			
			$total_data = array();					
			$total = 0;
			$taxes = $this->cart->getTaxes();
			
			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$sort_order = array(); 
				
				$results = $this->model_setting_extension->getExtensions('total');

				/*echo "<pre>";
				print_r($results);*/
				
				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}
				
				array_multisort($sort_order, SORT_ASC, $results);
				
				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('total/' . $result['code']);
			
						$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
					}
					
					$sort_order = array(); 
				  
					foreach ($total_data as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}
		
					array_multisort($sort_order, SORT_ASC, $total_data);			
				}
			}
			
			//$total_data['value'] = 

			

			foreach ($total_data as $key => $value) {
				$total_data[$key]['text'] = $this->somaValor($total_data[$key]['text']);
				if($product_quantity >=13 && $product_quantity <=17){
					$total_data[$key]['value'] += 16;
				}
				else if($product_quantity>=18 && $product_quantity <=30){
					$total_data[$key]['value'] += 18;
				}
				else if($product_quantity>=31 && $product_quantity <=53){
					$total_data[$key]['value'] += 28;
				}
			}

			//echo "<pre>";
			//print_r($total_data);

			$this->data['totals'] = $total_data;

			
						
			$this->data['continue'] = $this->url->link('common/home');
						
			$this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

            $this->load->model('setting/extension');
            
            $this->data['checkout_buttons'] = array();

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/checkout/cart.tpl';
			} else {
				$this->template = 'default/template/checkout/cart.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_bottom',
				'common/content_top',
				'common/footer',
				'common/header'	
			);
						
			$this->response->setOutput($this->render());					
    	} else {
      		$this->data['heading_title'] = $this->language->get('heading_title');

      		$this->data['text_error'] = $this->language->get('text_empty');

      		$this->data['button_continue'] = $this->language->get('button_continue');
			
      		$this->data['continue'] = $this->url->link('common/home');

			unset($this->session->data['success']);

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
					
			$this->response->setOutput($this->render());			
    	}
  	}
	
	protected function somaValor($valorText){
		$valor = str_replace('R$', '', $valorText);
		$v = explode(',', $valor);
		$valor_final = "R$".($v['0']+18).",".$v['1'];
		return $valor_final;
	}

	protected function validateCoupon() {
		$this->load->model('checkout/coupon');
				
		$coupon_info = $this->model_checkout_coupon->getCoupon($this->request->post['coupon']);			
		
		if (!$coupon_info) {			
			$this->error['warning'] = $this->language->get('error_coupon');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	protected function validateVoucher() {
		$this->load->model('checkout/voucher');
				
		$voucher_info = $this->model_checkout_voucher->getVoucher($this->request->post['voucher']);			
		
		if (!$voucher_info) {			
			$this->error['warning'] = $this->language->get('error_voucher');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	protected function validateReward() {
		$points = $this->customer->getRewardPoints();
		
		$points_total = 0;
		
		foreach ($this->cart->getProducts() as $product) {
			if ($product['points']) {
				$points_total += $product['points'];
			}
		}	
				
		if (empty($this->request->post['reward'])) {
			$this->error['warning'] = $this->language->get('error_reward');
		}
	
		if ($this->request->post['reward'] > $points) {
			$this->error['warning'] = sprintf($this->language->get('error_points'), $this->request->post['reward']);
		}
		
		if ($this->request->post['reward'] > $points_total) {
			$this->error['warning'] = sprintf($this->language->get('error_maximum'), $points_total);
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	protected function validateShipping() {
		if (!empty($this->request->post['shipping_method'])) {
			$shipping = explode('.', $this->request->post['shipping_method']);
					
			if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {			
				$this->error['warning'] = $this->language->get('error_shipping');
			}
		} else {
			$this->error['warning'] = $this->language->get('error_shipping');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
								
	public function add() {
		$this->language->load('checkout/cart');
		
		$json = array();
		
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
						
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		//selecionar a categoria do produto selecionado
		$category_id_product = $this->model_catalog_product->getCategories($product_id);

		$category_id_product = $this->model_catalog_product->getCategoryIdByCategoryId($category_id_product);


		//query para selecionar produto do carinho
		$products = $this->cart->getProducts();
		
		//echo count($products);
		/*echo "<pre>";
		print_r($products);
		exit;*/
		if(count($products)){
			$count = 0;
			foreach ($products as $product) {
				if($count == 0){
					$category_id_product_cart = $this->model_catalog_product->getCategories($product['product_id']);			
					$category_id_product_cart = $this->model_catalog_product->getCategoryIdByCategoryId($category_id_product_cart);
				$count .= 1;
				}
			}
		}
		else{
			$category_id_product_cart = 0;
		}
		//exit; 
		if($category_id_product_cart == 0){
			$verificar = true;
			$status = "ok";
			$msg = "Adicionar Produto";
			//$array = {"status":"ok", "msg": "Adicionar Produto"};
		}else if ($category_id_product_cart == 102){
			if($category_id_product == 102){
					// aceita
					$verificar = true;
					$status = "ok";
					$msg = "produto de cesta";
					//$array = {"status":"ok", "msg": "produto de delivery"};
				} else if($category_id_product == 60) {
					// nao aceita
					$verificar = false;
					$status = "error";
					$msg = "Você não pode misturar Delivery com Monte sua Cesta !";
					//$array = {"status":"error", "msg": "produto não e de delivery, ja existe um produto de delivery no carinho"};
				} else {
					// nao aceita
					$verificar = false;
					$status = "error";
					$msg = "Você não pode misturar Produtos de encomenda com Monte sua Cesta !";
				}
		}else{
			if($category_id_product_cart == 60){
				if($category_id_product == 60){
					// aceita
					$verificar = true;
					$status = "ok";
					$msg = "produto de delivery";
					//$array = {"status":"ok", "msg": "produto de delivery"};
				} else if($category_id_product == 102){
					// nao aceita
					$verificar = false;
					$status = "error";
					$msg = "Você não pode misturar Monte sua Cesta com Delivery !";
					//$array = {"status":"error", "msg": "produto não e de delivery, ja existe um produto de delivery no carinho"};
				} else  {
					// nao aceita
					$verificar = false;
					$status = "error";
					$msg = "Você não pode misturar Produtos de encomenda com Delivery !";
					//$array = {"status":"error", "msg": "produto não e de delivery, ja existe um produto de delivery no carinho"};
				}
			} else {
				if($category_id_product == 60){
					// nao aceita
					$verificar = false;
					$status = "error";
					$msg = "Você não pode misturar Delivery com Produtos de encomenda !";
					//$array = {"status":"error", "msg": "produto e de delivery, ja existe um produto que não e de delivery no carinho"};
				}else if($category_id_product == 102){
					// nao aceita
					$verificar = false;
					$status = "error";
					$msg = "Você não pode misturar Monte sua Cesta com Produtos de encomenda !";
					//$array = {"status":"error", "msg": "produto não e de delivery, ja existe um produto de delivery no carinho"};
				} else {
					// aceita
					$verificar = true;
					$status = "ok";
					$msg = "produto normal";
					//$array = {"status":"ok", "msg": "produto normal"};
				}
			}
		}

		/*echo "<pre>";
		print_r($category_id_product);
		echo "<br>";
		echo "<pre>";
		print_r($category_id_product_cart);	*/	

		if ($product_info) {			
			if (isset($this->request->post['quantity'])) {
				$quantity = $this->request->post['quantity'];
			} else {
				$quantity = 1;
			}
														
			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();	
			}
            
            if (isset($this->request->post['profile_id'])) {
                $profile_id = $this->request->post['profile_id'];
            } else {
                $profile_id = 0;
            }
			
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			
			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}
            
            $profiles = $this->model_catalog_product->getProfiles($product_info['product_id']);
            
            if ($profiles) {
                $profile_ids = array();
                
                foreach ($profiles as $profile) {
                    $profile_ids[] = $profile['profile_id'];
                }
                
                if (!in_array($profile_id, $profile_ids)) {
                    $json['error']['profile'] = $this->language->get('error_profile_required');
                }
            }
			
			if (!$json) {
				if($verificar == true){
					$this->cart->add($this->request->post['product_id'], $quantity, $option, $profile_id);

					$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));
					
					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
					
					// Totals
					$this->load->model('setting/extension');
					
					$total_data = array();					
					$total = 0;
					$taxes = $this->cart->getTaxes();
					
					// Display prices
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$sort_order = array(); 
						
						$results = $this->model_setting_extension->getExtensions('total');
						
						foreach ($results as $key => $value) {
							$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
						}
						
						array_multisort($sort_order, SORT_ASC, $results);
						
						foreach ($results as $result) {
							if ($this->config->get($result['code'] . '_status')) {
								$this->load->model('total/' . $result['code']);
					
								$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
							}
							
							$sort_order = array(); 
						  
							foreach ($total_data as $key => $value) {
								$sort_order[$key] = $value['sort_order'];
							}
				
							array_multisort($sort_order, SORT_ASC, $total_data);			
						}
					}
					$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
				}
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}
		$json['status'] = $status;
		$json['msg'] = $msg;
		$this->response->setOutput(json_encode($json));		
	}
	
	public function quote() {
		$this->language->load('checkout/cart');
		
		$json = array();	
		
		if (!$this->cart->hasProducts()) {
			$json['error']['warning'] = $this->language->get('error_product');				
		}				

		if (!$this->cart->hasShipping()) {
			$json['error']['warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));				
		}				
		
		if ($this->request->post['country_id'] == '') {
			$json['error']['country'] = $this->language->get('error_country');
		}
		
		if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '') {
			$json['error']['zone'] = $this->language->get('error_zone');
		}
			
		$this->load->model('localisation/country');
		
		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
		
		if ($country_info && $country_info['postcode_required'] && (utf8_strlen($this->request->post['postcode']) < 2) || (utf8_strlen($this->request->post['postcode']) > 10)) {
			$json['error']['postcode'] = $this->language->get('error_postcode');
		}
						
		if (!$json) {		
			$this->tax->setShippingAddress($this->request->post['country_id'], $this->request->post['zone_id']);
		
			// Default Shipping Address
			$this->session->data['shipping_country_id'] = $this->request->post['country_id'];
			$this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
			$this->session->data['shipping_postcode'] = $this->request->post['postcode'];
		
			if ($country_info) {
				$country = $country_info['name'];
				$iso_code_2 = $country_info['iso_code_2'];
				$iso_code_3 = $country_info['iso_code_3'];
				$address_format = $country_info['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';	
				$address_format = '';
			}
			
			$this->load->model('localisation/zone');
		
			$zone_info = $this->model_localisation_zone->getZone($this->request->post['zone_id']);
			
			if ($zone_info) {
				$zone = $zone_info['name'];
				$zone_code = $zone_info['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}	
		 
			$address_data = array(
				'firstname'      => '',
				'lastname'       => '',
				'company'        => '',
				'address_1'      => '',
				'address_2'      => '',
				'postcode'       => $this->request->post['postcode'],
				'city'           => '',
				'zone_id'        => $this->request->post['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $this->request->post['country_id'],
				'country'        => $country,	
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format
			);
		
			$quote_data = array();
			
			$this->load->model('setting/extension');
			
			$results = $this->model_setting_extension->getExtensions('shipping');
			
			
			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('shipping/' . $result['code']);
					
					$quote = $this->{'model_shipping_' . $result['code']}->getQuote($address_data); 
		
					if ($quote) {
						$quote_data[$result['code']] = array( 
							'title'      => $quote['title'],
							'quote'      => $quote['quote'], 
							'sort_order' => $quote['sort_order'],
							'error'      => $quote['error']
						);
					}
				}
			}

	
			$sort_order = array();
		  
			foreach ($quote_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}
	
			array_multisort($sort_order, SORT_ASC, $quote_data);
			
			$this->session->data['shipping_methods'] = $quote_data;

			$this->load->model('catalog/product');

			// Validate minimum quantity requirments.			
			$products = $this->cart->getProducts();

			//Verificar se o produto pertence a categoria de delivery 
			$this->load->model('catalog/product');
			$count = 0;
			foreach ($products as $product) {
				if($count == 0){
					$category_id_product_cart = $this->model_catalog_product->getCategories($product['product_id']);			
					$category_id_product_cart = $this->model_catalog_product->getCategoryIdByCategoryId($category_id_product_cart);
					$count .= 1;
				}
			}
			
			if ($this->session->data['shipping_methods']) {
				//echo "<pre>";
				//print_r($category_id_product_cart);
				if($category_id_product_cart != 60){ 
					$json['shipping_method'] = $this->session->data['shipping_methods'];
				} else {
					$json['shipping_method'] = $this->verificarCep($this->session->data['shipping_methods'], $this->request->post['postcode']);
				}
			} else {
				$json['error']['warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
			}				
		}	
		//$json['verificacao'] = "teste 123";
		//echo "<pre>";
		//print_r($quote['code']);
		//print_r($json['verificacao']);
		//exit;
		
		$this->response->setOutput(json_encode($json));						
	}

	//Verificar se CEP inserido é de Delivery
	public function verificarCep($shipping_methods, $cep){

		$cep_gavea = "22451740,22431008,22451000,22450060,22451264,22451265,22451262,22451263,22610170,22451240,22451420,22451190,22470060,22451360,22451120,22451230,22451200,22451160,22451020,22451385,22451380,22450290,22451440,22451445,22451550,22451310,22451320,22610320,22451600,22451060,22451330,22451400,22451405,22451050,22451090,22451080,22451300,22451180,22451110,22451010,22451210,22451100,22470100,22470070,22451042,22451045,22451047,22451044,22451041,22451040,22451220,22430150,22470090,22451170,22451130,22451030,22451500,22451150,22610030,22451430,22451280,22451270,22451250,22451290,22451570,22451455,22451450,22451070,22451370,22451610,22451140,22451560,22451520,22451630,22451266,22451271,22451272,22451269";

		$cep_sao_conrado = "22610070,22610242,22610010,22610050,22450221,22610095,22610210,22610002,22610001,22610142,22610141,22610235,22610060,22610230,22610390,22610215,22610310,22610240,22610340,22610370,22610260,22610180,22610100,22610290,22610360,22610350,22610020,22610040,22610270,22610120,22610110,22610225,22610380,22610150,22610130,22610190,22610160,22610080,22610200,22610250,22610300,22610220,22610280,22610330";

		$cep_jardim_botanico = "22470040,22460320,22470180,22460120,22461140,22461080,22461090,22460240,22460250,22461160,22460280,22461120,22461190,22460230,22460130,22461060,22460050,22461050,22460200,22470150,22461170,22460090,22460220,22261070,22461200,22461020,22460210,22461210,22460110,22461030,22461130,22470051,22460000,22461000,22470050,22470110,22460190,22460170,22460011,22460012,22460014,22460010,22461152,22461151,22460290,22460260,22461230,22461260,22460300,22461110,22461100,22461070,22460310,22460036,22460035,22460030,22460100,22461240,22460060,22460080,22460180,22461250,22461040,22461010,22460020,22460140,22460040,22460070";

		$cep_leblon = "22430060,22440033,22440035,22440034,22440032,22431003,22431001,22431002,22431004,22430041,22430042,22441000,22441014,22441015,22441012,22441013,22450000,22450001,22450003,22450002,22431020,22440020,22431080,22441130,22431060,22430140,22430080,22430120,22441050,22430180,22430030,22450040,22450050,22450070,22441040,22450170,22450160,22440000,22440005,22450190,22440050,22431010,22440040,22450100,22430200,22441030,22431030,22431050,22441060,22450090,22450150,22430170,22450080,22450210,22441140,22431040,22441090,22430070,22430190,22450200,22450110,22431070,22441100,22430210,22430220,22441080,22441020,22450120,22430160,22430110,22430100,22441025,22430090,22430050,22441110,22450030,22450180,22430130,22441120,22440060,22450020,22450140,22450130,22441070";

		$cep_ipanema = "22410090,22411071,22411072,22080050,22410060,22081041,22081042,22420002,22420006,22420004,22420012,22420008,22410070,22430010,22080070,22410020,22430000,22410030,22430020,22411030,22411040,22410050,22411020,22081060,22411003,22411002,22411001,22411000,22421000,22081040,22411060,22420020,22080046,22080045,22421010,22071110,22411050,22420010,22420030,22080060,22410040,22421029,22421023,22421022,22421024,22421025,22421027,22421028,22421026,22410080,22420043,22420041,22420042,22420040,22421030,22410010,22411010,22410000,22410002,22410003,22410001,22081050";

		$cep_lagoa = "22470220,22470040,22471211,22470001,22470002,22470003,22470004,22471006,22471004,22471002,22471003,22410100,22471060,22210000,22471110,22471310,22471100,22411080,22470190,22471090,22471140,22471150,22471270,22470120,22471340,22471220,22471240,22471120,22470230,22471170,22471210,22470210,22471250,22471330,22470010,22470170,22471160,22470130,22470201,22470202,22471080,22471130,22071050,22470240,22461220,22471230,22471180,22470030,22471070,22471200,22471190,22470280,22470290,22470300";

		$cep_humaita = "22261170,22271070,22260210,22261130,22261110,22260160,22271040,22261160,22261030,22261010,22271060,22261100,22261003,22261004,22261002,22261005,22261000,22261001,22261140,22261150,22271050,22261120,22261040,22271080,22261080,22260220,22261020,22260240,22261090,22271042,22271041,22271044,22271043,22261060,22261050,22270007,22270018";

		$cep_copacabana = "22010000,22021000,22021001,22041001,22070000,22070001,22070002,22061030,22060001,22070011,22031000,22050001,22050002,22060002,22070012,22020001,22020002,22011040,22011010,22081032,22081031,22031130,22011080,22031112,22031111,22040001,22040030,22070020,22011020,22020030,22041050,22061040,22020070,22040050,22031060,22060030,22060040,22041080,22030010,22050032,22050031,22051001,22051002,22011001,22040001,22011002,22040002,22020010,22061020,22081000,22041090,22020050,22051030,22021050,22081020,22051012,22051011,22071120,22041040,22051020,22071020,22050012,22050011,22020020,22031020,22061060,22031100,22011030,22021030,22031012,22031011,22080040,22080041,22080010,22011050,22011060,22290110,22290120,22011090,22041020,22040020,22020060,22080030,22031040,22081025,22041030,22060020,22041070,22030020,22030040,22071000,22031050,22021010,22030030,22040010,22071040,22071090,22061000,22071030,22040041,22040042,22080002,22080001,2021040,22020040,22021020,22071100,22071060,22041012,22041011,22031030,22031072,22031071,22081010,22041060,22031090,22030002,22030001,22061010,22050020,22071010,22071080,22071070,22061080,22011100,22030050,22061070,22061050,22031120,22061021,22081001,22051021,22040021,22060021,22071001,22021040,22071101,22061011";

		$cep_leme = "22010121,22010011,22010122,22010060,22010020,22010070,22010100,22010040,22010050,22010010,22010030,22010110";

		$cep_botafogo = "22290090,22260105,22260131,22260177,22250150,22290070,22290240,22290210,22290140,22260191,22260152,22260135,22260188,22260137,22260189,22260192,22260157,22260168,22260124,22260148,22260139,22260134,22260183,22260180,22260149,22290130,22281132,22281131,22250170,22260184,22231050,22250000,22231030,22260130,22260181,22290050,22250200,22290150,22260206,22250010,22290060,22290230,22260070,22250145,22250040,22260155,22270080,22280110,22260050,22260166,22281110,22280070,22260154,22280080,22251030,22251050,22231000,22260020,22260120,22290190,22260205,22260167,22260199,22270020,22271000,22260193,22260169,22230090,22250160,22271020,22260195,22281010,22260122,22260100,22260138,22260187,22290030,22290031,22260201,22260164,22260186,22260182,22260125,22270070,22260179,22280030,22281100,22260153,22260136,22260145,22260162,22260146,22260156,22260142,22260172,22280020,22260176,22260196,22281150,22231060,22260090,22280060,22260010,22231020,22290000,22231040,22281090,22290080,22280003,22280002,22280005,22280004,22290040,22281020,22270060,22281060,22281050,22260190,22281040,22260197,22251010,22260194,22231010,22260128,22251000,22260150,22281140,22290160,22290170,22260129,22260140,22251060,22290175,22251040,22271010,22271100,22260126,22260040,22281000,22251020,22251090,22250210,22260005,22260132,22260143,22280090,22260127,22260203,22270050,22280010,22260185,22281080,22281070,22251080,22280040,22260123,22281035,22281034,22281036,22281033,22281032,22260147,22280100,22260007,22260001,22260009,22260004,22260006,22260002,22260000,22260003,22270030,22290010,22260170,22260110,22271110,22280050,22260030,22260133,22260178,22251070,22281120,22271021,22271022,22250180,22271091,22271092,22270001,22270000,22270016,22270012,22270010,22270003,22270014,22270005,22250140,22260174,22260158,22250190,22280120,22270040,22280130,22290020,22260080";

		$cep_laraneiras = "22231230,22240180,22240090,22240120,22240030,22231210,22245130,22240160,22231170,22240130,22241020,22240080,22240170,22231220,22245020,22245070,22221110,22245000,22231110,22245030,22245060,22240005,22240006,22240003,22240004,22240000,22221120,22221130,22240040,22231160,22240020,22221070,22245110,22245120,22221100,22231120,22245140,22240100,22231070,22245050,22241000,22231100,22221080,22231200,22245010,22221090,22221140,22231090,22240150,22231080,22245040,22245100,22245150,22240060,22240140,22241010,22240110,22231180,22240070,22245080,22245090,22221150,22240010,22231190";

		$cep_flamengo = "22250060,22250020,22250030,22230020,22210065,22210030,22210060,22250110,22220080,22220030,22231140,22210050,22230010,22250130,22220050,22220040,22230040,22210040,2250100,22250120,22220060,22230061,22230060,22230030,22231150,22210085,22210080,22230080,22250090,22250070,22231130,22250080,22230000,22230001,22221000,22210070,22230070,22230050";

		$cep_catete = "22221020,22220000,22220010,22220070,22221010,22221011,22221040,22220001,22221060,22211200,22221030,22221050,22220020,22221036,22221034,22221038,22211210,22221032";

		$cep_gloria = "22211130,20021040,20021140,20021030,22211120,22211100,22210015,20241170,22211110,20241190,22210020,22210005,20021100,20021300,22211140,22211150,20241150,20241220,22211180,20241180,22220000,20241160,22210010,22211160,22211190,20021150,22211170,22211230,22211090";


		$cepdelivery = $cep_gavea.",".$cep_sao_conrado.",".$cep_jardim_botanico.",".$cep_leblon.",".$cep_ipanema.",".$cep_lagoa.",".$cep_humaita.",".$cep_copacabana.",".$cep_leme.",".$cep_botafogo.",".$cep_laraneiras.",".$cep_flamengo.",".$cep_catete.",".$cep_gloria;


		$arquivo = explode(",", $cepdelivery);
			/*echo "<pre>";
			print_r($shipping_methods);
			exit;*/

		if(in_array($cep, $arquivo)){
			//echo "pertence";
			unset($shipping_methods['pickup']);
			return $shipping_methods;
		}
		else{
			//echo "não pertence";
			unset($shipping_methods['category_product_based']);
			unset($shipping_methods['pickup']);
			//echo "<pre>";
			//print_r($shipping_methods);
			return $shipping_methods;
			
		}

	}

	
	public function country() {
		$json = array();
		
		$this->load->model('localisation/country');

    	$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);
		
		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']		
			);
		}
		
		$this->response->setOutput(json_encode($json));
	}
}
?>
