<?php
// Text
$_['text_popup_message']           = 'Deixe-nos saber sua data de nascimento e você poderá receber um presente de surpresa em seu aniversário.';
$_['text_dont_show_again']         = 'Não me mostrar novamente este formulário';
$_['text_success_birthday_add']    = 'Seu aniversário foi salvo!';
$_['text_success_birthday_update'] = 'Seu aniversário foi atualizada!';

// Entry
$_['entry_year']                   = 'Ano: ';
$_['entry_month']                  = 'Mês: ';
$_['entry_day']                    = 'Dia: ';

// Button
$_['button_no_thanks']             = 'Não, obrigado!';
$_['button_set_birthday']          = 'Definir<br />aniversário';

//Error

$_['error_incorrect_birthdate']    = 'Data de nascimento incorreto! Por favor cheque novamente.';
?>