<?php
// Text
$_['text_information']  = 'Informação';
$_['text_service']      = 'Atendimento';
$_['text_extra']        = 'Outros';
$_['text_account']      = 'Conta';
$_['text_contact']      = 'Contate-nos';
$_['text_return']       = 'Devoluções';
$_['text_sitemap']      = 'Nossas lojas';
$_['text_manufacturer'] = 'Fabricantes';
$_['text_voucher']      = 'Vale Presente';
$_['text_affiliate']    = 'Afiliados';
$_['text_special']      = 'Promoções';
$_['text_account']      = 'Minha conta';
$_['text_order']        = 'Histórico de pedidos';
$_['text_wishlist']     = 'Lista de desejos';
$_['text_newsletter']   = 'Boletim Informativo';
$_['text_powered']      = 'Desenvolvido por <a href="http://www.clicksoft.com.br">Clicksoft</a><br /> %s &copy; %s';
?>
