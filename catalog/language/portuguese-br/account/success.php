<?php
// Heading
$_['heading_title'] = 'Confirmação';

// Text
$_['text_message']  = '<p>Obrigada por se cadastrar em nossa loja. 
Aproveite os privilégios que só os clientes cadastrados tem acesso e melhore sua experiência online conosco. 
Um email de confirmação foi enviado para o e-mail que você utilizou no cadastro de sua conta. Se tiver alguma dúvida, entre em contato conosco.
</p><br />';
$_['text_approval'] = '<p>Obrigado por se registar na loja %s!</p><p>Você receberá uma notificação por e-mail assim que sua conta for ativada pelo atendimento.</p><p>Se você tiver qualquer dúvidas sobre o funcionamento desta loja online, por favor, <a href="%s">contate-nos</a>.</p>';
$_['text_account']  = 'Conta';
$_['text_success']  = 'Sucesso';
?>