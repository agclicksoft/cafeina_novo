<?php
// Text
$_['text_subject']              = 'Cupom aniversário - relatório Admin';
$_['text_hi']                   = 'Olá Administrador,';
$_['text_info']                 = 'Abaixo você pode encontrar detalhes sobre o cupom de aniversário que sera enviado para seus clientes.';
$_['text_percent']              = 'por cento';
$_['text_fixed']                = 'fixo';

//Column
$_['column_customer']           = 'Cliente';
$_['column_code']               = 'Código';
$_['column_type']               = 'Tipo';
$_['column_discount']           = 'desconto';
$_['column_total']              = 'Required cart total';
$_['column_validity']           = 'Válido até ..';
?>