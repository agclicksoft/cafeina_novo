<?php
// Text
$_['text_subject']              = 'Birthday Coupon - Admin report';
$_['text_hi']                   = 'Hi Admin,';
$_['text_info']                 = 'Bellow you can find details about birthday coupon send today to your customers.';
$_['text_percent']              = 'percent';
$_['text_fixed']                = 'fixed';

//Column
$_['column_customer']           = 'Customer';
$_['column_code']               = 'Code';
$_['column_type']               = 'Type';
$_['column_discount']           = 'Discount';
$_['column_total']              = 'Required cart total';
$_['column_validity']           = 'Valid until..';
?>