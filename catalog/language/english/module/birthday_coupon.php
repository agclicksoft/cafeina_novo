<?php
// Text
$_['text_popup_message']           = 'Let us to know your birth date and you can receive surprise gift on your birthday.';
$_['text_dont_show_again']         = 'Don\'t show me again this form';
$_['text_success_birthday_add']    = 'Your birthday was saved!';
$_['text_success_birthday_update'] = 'Your birthday was updated!';

// Entry
$_['entry_year']                   = 'Year: ';
$_['entry_month']                  = 'Month: ';
$_['entry_day']                    = 'Day: ';

// Button
$_['button_no_thanks']             = 'No, thanks!';
$_['button_set_birthday']          = 'Set<br />birthday';

//Error

$_['error_incorrect_birthdate']    = 'Incorrect birthdate! Please check again.';
?>