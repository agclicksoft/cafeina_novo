<style type="text/css">
.triangle-up-right:after {
    border-top: 50px solid <?php echo $badge_color; ?> !important;
}
.triangle-down-right:after {
    border-right: 50px solid <?php echo $badge_color; ?> !important;
}
.triangle-down-left:after {
    border-bottom: 50px solid <?php echo $badge_color; ?> !important;
}
.triangle-up-left:after {
    border-left: 50px solid <?php echo $badge_color; ?> !important;
}
</style>

<a class="triangle-<?php echo $badge_position; ?> pos-fixed-<?php echo $badge_position; ?> birthday-popup-trigger" href="#responsive-birthday-popup"></a>
<a class="bell bell-<?php echo $badge_position; ?> birthday-popup-trigger" href="#responsive-birthday-popup"></a>

<div id="responsive-birthday-popup" class="birthday-popup zoom-anim-dialog mfp-hide">
	<div class="popup-notification"></div>
	<div class="popup-message"><?php echo $text_popup_message; ?></div>
	<div class="popup-date-area">
		<div class="date-column">
			<?php echo $entry_day; ?><br />
			<select name="birth_day">
			<?php for ($day = 1; $day <= 31; $day++) { ?>
			<?php if ($day == $birth_day) { ?>
			<option value="<?php echo $day; ?>" selected="selected"><?php echo $day; ?></option>
			<?php } else { ?>
			<option value="<?php echo $day; ?>"><?php echo $day; ?></option>
			<?php } ?>
			<?php } ?>
			</select>
		</div>	
		
		<div class="date-column">
			<?php echo $entry_month; ?><br />
			<select name="birth_month">
			<?php for ($month = 1; $month <= 12; $month++) { ?>
			<?php if ($month == $birth_month) { ?>
			<option value="<?php echo $month; ?>" selected="selected"><?php echo $month; ?></option>
			<?php } else { ?>
			<option value="<?php echo $month; ?>"><?php echo $month; ?></option>
			<?php } ?>
			<?php } ?>
			</select>
		</div>	

		<div class="date-column">
			<?php echo $entry_year; ?><br />
			<select name="birth_year">
			<?php for ($year = $current_year; $year > 1900; $year--) { ?>
			<?php if ($year == $birth_year) { ?>
			<option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
			<?php } else { ?>
			<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
			<?php } ?>
			<?php } ?>
			</select> 	
		</div>

		<div class="date-column">
		<a class="set-birthday" id="set-birthday"><?php echo $button_set_birthday; ?></a>
		</div>	
	</div>
	<div class="popup-dont-show"><label id="popup-dont-show">[ <?php echo $text_dont_show_again; ?> ]</div>
</div>

<script type="text/javascript"><!--
$(document).ready(function() {
	$('.birthday-popup-trigger').magnificPopup({
		type: 'inline',
		fixedContentPos: true,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		closeOnBgClick: false,
		enableEscapeKey: false,
		preloader: true,
		removalDelay: 500,
		mainClass: 'my-mfp-slide-bottom',
		callbacks: {
		    beforeOpen: function() {
				$('.popup-notification .success, .popup-notification .warning, .popup-notification .error').remove();
				$.ajax({
					url: 'index.php?route=module/birthday_coupon/addautoopencount',
					success: function() {
						
					}
				});
		    }
		}
	});
	
	<?php if ($auto_open) { ?>
	$('.birthday-popup-trigger').trigger('click');
	<?php } ?>
	
	$('#popup-dont-show').bind('click', function(){
		$.ajax({
			url: 'index.php?route=module/birthday_coupon/dontshowpopup',
			success: function() {
				$('.mfp-close').trigger('click');
			}
		});
	});
	
	$('#set-birthday').bind('click', function() {
		$.ajax({
			type: 'POST',
			url: 'index.php?route=module/birthday_coupon/setbirthdate',
			data: $('.birthday-popup select'),
			dataType: 'json',
			success: function(json) {
				$('.popup-notification .success, .popup-notification .warning, .popup-notification .error').remove();
				
				if (json['error']) {
					if (json['error']['warning']) {
						$('.popup-notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
						$('.popup-notification .warning').fadeIn('slow');
					}
				}
				
				if (json['success']) {
					$('.popup-notification').html('<div class="success" style="display: none;">' + json['success'] + '</div>');
					$('.popup-notification .success').fadeIn('slow');
					
					setTimeout(function() {
						$('.mfp-close').trigger('click');
					}, 2000); 
				}
			}
		});
	});
});
--></script>