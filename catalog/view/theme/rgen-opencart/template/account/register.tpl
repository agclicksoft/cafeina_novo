<?php echo $header; ?>
<style type="text/css">
	#postcode{
			background-image: none;
		    border: 2px solid #f0f2dc;
		    border-radius: 6px;
		    background-color: #fff;
		    width: 340px;
		    padding: 3px 8px;
		    height: 24px;
	}
	
	@media only screen and (max-width: 767px) and (min-width: 200px){
		#postcode{
			width: 90%;
		}
	}

</style>
<div id="content">
	
	<!--CONTENT LEFT -->
	<?php echo $column_left; ?>
	
	<!--CONTENT RIGHT -->
	<?php echo $column_right; ?>
	
	<!--PAGE CONTENT WRAPPER -->
	<div class="content-body register">
		
		<div class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<span><?php echo $breadcrumb['separator']; ?></span><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
			<?php } ?>
		</div>
		
		<h1 class="page-heading"><strong><?php echo $heading_title; ?></strong></h1>
		<?php if ($error_warning) { ?>
			<div class="warning"><?php echo $error_warning; ?></div>
		<?php } ?>
	
		<?php echo $content_top; ?>
		
		<p><?php echo $text_account_already; ?></p>
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
		<script type="text/javascript">
		    function validarformu(email){
		    	var verificado = email;
		    	corrigido = verificado.replace(/[' 'çÇáâĩũĨŨãẽõêôÔÊÂÃẼÕÁàÀéèÉÈíìÍÌóòÓÒúùÚÙñÑ!#$%*()+~}{&]/g,'');
		    	if(corrigido == email){
		    		$("#email").val(corrigido);
		    		$("#erro-email").css("display","none");
		    		console.log(email);
		    	} else {
		    		console.log(email);
		    		$("#email").val("");
		    		$("#erro-email").css("display","block");
		    	}    	
		    }

		    function validarPhone(phone){
		    	var verificado2 = phone;
		    	corrigido2 = verificado2.replace(/[' 'aAbBcCdDeEfFgGHhiIjJkKlLmMnNoOpPqQrRsStTuUvVxXwWyYzZ!@#$%*-+=_.,çÇáâĩũĨŨãẽõêôÔÊÂÃẼÕÁàÀéèÉÈíìÍÌóòÓÒúùÚÙñÑ!#$%*()+~}{&]/g,'');
		    	if(corrigido2.length < 8){
		    		console.log(phone);
		    		$("#phone").val("");
		    		$("#erro-phone").html("Insira no mínimo 8 dígitos");
		    		$("#erro-phone").css("display","block");
		    	}else {
		    		if(corrigido2 == phone){
			    		$("#phone").val(corrigido2);
			    		$("#erro-phone").css("display","none");
			    		console.log(phone);
			    	} else {
			    		console.log(phone);
			    		$("#phone").val("");
			    		$("#erro-phone").html("Insira somente números!");
			    		$("#erro-phone").css("display","block");
		    		}
		    	}
			}
    	</script>
		<div class="box-form">
			<h2 class="header-3"><?php echo $text_your_details; ?></h2>
			<div class="content">
				<table class="form">
					<tr>
						<td><?php echo $entry_firstname; ?> <span class="required">*</span></td>
						<td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
							<?php if ($error_firstname) { ?>
							<span class="error"><?php echo $error_firstname; ?></span>
							<?php } ?></td>
					</tr>
					<tr>
						<td><?php echo $entry_lastname; ?> <span class="required">*</span></td>
						<td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
							<?php if ($error_lastname) { ?>
							<span class="error"><?php echo $error_lastname; ?></span>
							<?php } ?></td>
					</tr>
					<tr>
						<td><?php echo $entry_email; ?> <span class="required">*</span>
						<span id="erro-email" style="color:#FF0000;display:none;">O email não pode ter caracteres especiais!</span></td>
						<td><input type="text" name="email" id="email" onblur="validarformu(this.value)" value="<?php echo $email; ?>" />
							<?php if ($error_email) { ?>
							<span class="error"><?php echo $error_email; ?></span>
							<?php } ?></td>
					</tr>
					<tr>
						<td><?php echo $entry_telephone; ?><span class="required">*</span></td>
						<td><span class="required">*Insira DDD + telefone somente com números</span><span id="erro-phone" style="color:#FF0000;display:none;">Insira somente números!</span><br><input type="text" name="telephone" value="<?php echo $telephone; ?>" class="large-field" id="phone" maxlength="11" onblur="validarPhone(this.value);"/>
						<?php if ($error_telephone) { ?>
							<span class="error"><?php echo $error_telephone; ?></span>
							<?php } ?></td>
					</tr>
					<tr>
						<td><?php echo $entry_fax; ?></td>
						<td><input type="text" name="fax" value="<?php echo $fax; ?>" /></td>
					</tr>
				</table>
			</div>		
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				/*$("#company").hide("slow");
				$("#postcode").blur(function(){
	      			$("#company").show("slow");
	    		});*/ 
	    		//$("#postcode").mask("99999999")
	    		//verifica a quantide de elemtentos
	    		var inputQuantity = [];
			    $(function() {
					$("#postcode").each(function(i) {
					inputQuantity[i]=this.defaultValue;
					 $(this).data("idx",i); // save this field's index to access later
					});
					$("#postcode").on("keyup", function (e) {
					var $field = $(this),
					    val=this.value,
					    $thisIndex=parseInt($field.data("idx"),10); // retrieve the index
					//        window.console && console.log($field.is(":invalid"));
					  //  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
					if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
					    this.value = inputQuantity[$thisIndex];
					    return;
					} 
					if (val.length > Number($field.attr("maxlength"))) {
					  val=val.slice(0, 5);
					  $field.val(val);
					}
					inputQuantity[$thisIndex]=val;
					});      
			    });
			})

			/*document.getElementById("company").style.display="none";
		 function mostrar(){
		 	document.getElementById("company").style.display="block";
		 }*/
		</script>		
		<div class="box-form">
			<h2 class="header-3"><?php echo $text_your_address; ?> </h2>
			<div class="content">
				<table class="form">
					<!--1.5.3 -->
					<div id="esconder" style="display:none !important;">
					<tr style="display:none !important;">
					  <td><?php echo $entry_customer_group; ?></td>
					  <td>
						<?php foreach ($customer_groups as $customer_group) { ?>
						<?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
						<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
						<label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
						<br />
						<?php } else { ?>
						<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />
						<label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
						<br />
						<?php } ?>
						<?php } ?>
					  </td>
					</tr>         
					<tr style="display:none !important;">
					  <td><?php echo $entry_company_id; ?> <span id="company-id-required" class="required">*</span></td>
					  <td><input type="text" name="company_id" value="<?php echo $company_id; ?>" />
						<?php if ($error_company_id) { ?>
						<span class="error"><?php echo $error_company_id; ?></span>
						<?php } ?></td>
					</tr>
					<tr id="tax-id-display">
					  <td><?php echo $entry_tax_id; ?> <span id="tax-id-required" class="required">*</span></td>
					  <td><input type="text" name="tax_id" value="<?php echo $tax_id; ?>" />
						<?php if ($error_tax_id) { ?>
						<span class="error"><?php echo $error_tax_id; ?></span>
						<?php } ?></td>
					</tr>
					</div>
					<!--1.5.3 -->
					<tr>
						<td><?php echo $entry_postcode; ?> <span id="postcode-required" class="required">*</span></td>
						<td><input type="number" name="postcode" id="postcode" value="<?php echo $postcode; ?>" maxlength="8" min="0" max="99999999"/>  <span style="color:#FF0000;">*Insira somente números</span>
							<?php if ($error_postcode) { ?>
							<span class="error"><?php echo $error_postcode; ?></span>
							<?php } ?></td>
					</tr>
					<tr>
						<td><?php echo $entry_address_1; ?> <span class="required">*</span></td>
						<td><input type="text" name="address_1" value="<?php echo $address_1; ?>" />
							<?php if ($error_address_1) { ?>
							<span class="error"><?php echo $error_address_1; ?></span>
							<?php } ?></td>
					</tr>
					<tr id="company">
						<td><?php echo $entry_company; ?></td>
						<td><input type="text" name="company" value="<?php echo $company; ?>" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_address_2; ?></td>
						<td><input type="text" name="address_2" value="<?php echo $address_2; ?>" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_city; ?> <span class="required">*</span></td>
						<td><input type="text" name="city" value="<?php echo $city; ?>" />
							<?php if ($error_city) { ?>
							<span class="error"><?php echo $error_city; ?></span>
							<?php } ?></td>
					</tr>
					<tr style="display:none;">
						<td><?php echo $entry_country; ?> <span class="required">*</span></td>
						<td><select name="country_id">
								<option value=""><?php echo $text_select; ?></option>
								<?php foreach ($countries as $country) { ?>
								<?php if ($country['country_id'] == $country_id) { ?>
								<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
								<?php } else { ?>
								<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
								<?php } ?>
								<?php } ?>
							</select>
							<?php if ($error_country) { ?>
							<span class="error"><?php echo $error_country; ?></span>
							<?php } ?></td>
					</tr>
					<tr>
						<td><?php echo $entry_zone; ?> <span class="required">*</span></td>
						<td><select name="zone_id">
							</select>
							<?php if ($error_zone) { ?>
							<span class="error"><?php echo $error_zone; ?></span>
							<?php } ?></td>
					</tr>
				</table>
			</div>		
		</div>
		
		
		<div class="box-form">
			<h2 class="header-3"><?php echo $text_your_password; ?></h2>
			<div class="content">
				<table class="form">
					<tr>
						<td><?php echo $entry_password; ?> <span class="required">*</span></td>
						<td><input type="password" name="password" value="<?php echo $password; ?>" />
							<?php if ($error_password) { ?>
							<span class="error"><?php echo $error_password; ?></span>
							<?php } ?></td>
					</tr>
					<tr>
						<td><?php echo $entry_confirm; ?> <span class="required">*</span></td>
						<td><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
							<?php if ($error_confirm) { ?>
							<span class="error"><?php echo $error_confirm; ?></span>
							<?php } ?></td>
					</tr>
				</table>
			</div>	
		</div>
		
		<div class="box-form">
			<h2 class="header-3"><?php echo $text_newsletter; ?></h2>
			<div class="content">
				<table class="form">
					<tr>
						<td><?php echo $entry_newsletter; ?></td>
						<td><?php if ($newsletter) { ?>
							<input type="radio" name="newsletter" value="1"  />
							<?php echo $text_yes; ?>
							<input type="radio" name="newsletter" value="0" checked="checked"/>
							<?php echo $text_no; ?>
							<?php } else { ?>
							<input type="radio" name="newsletter" value="1" checked="checked"/>
							<?php echo $text_yes; ?>
							<input type="radio" name="newsletter" value="0"  />
							<?php echo $text_no; ?>
							<?php } ?></td>
					</tr>
				</table>
			</div>
		</div>
		
		
		<?php if ($text_agree) { ?>
		<div class="buttons">
			<table class="form">
				<tr>
					<td></td>
					<td>
						<?php if ($agree) { ?>
						<input type="checkbox" name="agree" value="1" checked="checked" />
						<?php } else { ?>
						<input type="checkbox" name="agree" value="1" />
						<?php } ?>
						<?php echo $text_agree; ?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="<?php echo $button_continue; ?>" class="button dark-bt" /></td>
				</tr>
			</table>
		</div>
		<?php } else { ?>
		<div class="buttons">
			<div class="right">
				<input type="submit" value="<?php echo $button_continue; ?>" class="button dark-bt" />
			</div>
		</div>
		<?php } ?>
		
	</form>
		
	</div>
	<?php echo $content_bottom; ?>

</div>

<script type="text/javascript"><!--





//--></script> 
<script type="text/javascript"><!--

$('input[name=\'customer_group_id\']:checked').live('change', function() {
	var customer_group = [];
	
<?php foreach ($customer_groups as $customer_group) { ?>
	customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
<?php } ?>	

	if (customer_group[this.value]) {
		if (customer_group[this.value]['company_id_display'] == '1') {
			$('#company-id-display').show();
		} else {
			$('#company-id-display').hide();
		}
		
		if (customer_group[this.value]['company_id_required'] == '1') {
			$('#company-id-required').show();
		} else {
			$('#company-id-required').hide();
		}
		
		if (customer_group[this.value]['tax_id_display'] == '1') {
			$('#tax-id-display').show();
		} else {
			$('#tax-id-display').hide();
		}
		
		if (customer_group[this.value]['tax_id_required'] == '1') {
			$('#tax-id-required').show();
		} else {
			$('#tax-id-required').hide();
		}	
	}
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>   
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
				
			}
			
			$('select[name=\'zone_id\']').html(html);
			
			
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
$(document).ready(function() {
	$('.colorbox').colorbox({
		width: 640,
		height: 480,
		onComplete:function(){
			$('#cboxLoadedContent').wrapInner('<div class="popup-box"></div>');
			$('#cboxLoadedContent').find('h1').addClass('page-heading').wrapInner('<strong></strong>');
		}
	});
});

//--></script> 
<script type="text/javascript" src="catalog/view/theme/rgen-opencart/js/maskedinput-1.1.2.pack.js"/>
<?php echo $footer; ?>