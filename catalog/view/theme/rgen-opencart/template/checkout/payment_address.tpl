
<div class="box-form">
	<?php if ($addresses) { ?>
	
	<input type="radio" name="payment_address" value="existing" id="payment-address-existing" checked="checked" />
	<label for="payment-address-existing"><strong><?php echo $text_address_existing; ?></strong></label>
	<div id="payment-existing">
		<select name="address_id" style="width: 100%; margin-bottom: 15px;" size="5">
			<?php foreach ($addresses as $address) { ?>
			<?php if ($address['address_id'] == $address_id) { ?>
			<option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
			<?php } else { ?>
			<option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
			<?php } ?>
			<?php } ?>
		</select>
	</div>

	<div>
		<input type="radio" name="payment_address" value="new" id="payment-address-new" />
		<label for="payment-address-new"><strong><?php echo $text_address_new; ?></strong></label>
	</div>
	<?php } ?>


	<script>


      function xFunc_justNumber( digit ) {
      	var xDigit = digit.keyCode;
      	if (xDigit == 0)
      		xDigit = digit.which;

      //	alert( xDigit );

			if ((xDigit > 47 && xDigit < 58) || (xDigit == 8 || xDigit == 9 || xDigit == 46))
				return true
			else
      		return false;
      }


      function xFunc_getCEP( cep ) {
			if (cep.length > 0) {
			//	alert( "http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep );

			//	cep = $.trim($('input[name="postcode"]').val().replace('-', ''));

				$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function() {
					if(resultadoCEP["resultado"] == "1"){
						$('input[name="address_1"]').val(unescape(resultadoCEP["tipo_logradouro"])+" "+unescape(resultadoCEP["logradouro"]));
						$('input[name="address_2"]').val(unescape(resultadoCEP["bairro"]));
						$('input[name="city"]').val(unescape(resultadoCEP["cidade"]));
					//	$('select[name=\'zone_id\']').load('index.php?route=account/register/estado_autocompletar&estado='+unescape(resultadoCEP["uf"]));			
					} else {
						alert("Endereço do cep não encontrado. Digite o endereço manualmente!");
					}
	     		}) 
			} else {
				$('input[name="address_1"]').val('');
				$('input[name="address_2"]').val('');
				$('input[name="city"]').val(''); 
			}
     	}

     	/*function consultacep(cep){
	      	$('#logradouro').val(null);
	      	$('#bairro').val(null);
	      	$('#localidade').val(null);
	    	$('#logradouro').attr('placeholder','Carregando...');
	      	$('#bairro').attr('placeholder','Carregando...');
	      	$('#localidade').attr('placeholder','Carregando...');

	      cep = cep.replace(/\D/g,"")
	      url="http://cep.correiocontrol.com.br/"+cep+".js"
	      s=document.createElement('script')
	      s.setAttribute('charset','utf-8')
	      s.src=url
	      document.querySelector('head').appendChild(s)
	    }
	 
	    function correiocontrolcep(valor){
	      if (valor.erro) {
	      	$('#logradouro').removeAttr('placeholder');
	      	$('#bairro').removeAttr('placeholder');
	      	$('#localidade').removeAttr('placeholder');
	      	$('#logradouro').val(null);
	      	$('#bairro').val(null);
	      	$('#localidade').val(null);
	        alert('Cep não encontrado');       
	        return;
	      };

	      document.getElementById('logradouro').value=valor.logradouro
	      document.getElementById('bairro').value=valor.bairro
	      document.getElementById('localidade').value=valor.localidade
	      //document.getElementById('uf').value=valor.uf
	    }*/
	</script>		

	
	<div id="payment-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
		<table class="form">
			<tr>
				<td><?php echo $entry_firstname; ?> <span class="required">*</span></td>
				<td><input type="text" name="firstname" value="" class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_lastname; ?> <span class="required">*</span></td>
				<td><input type="text" name="lastname" value="" class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_postcode; ?> <span class="required" id="payment-postcode-required">*</span></td>
				<td><input type="text" name="postcode" id="postcode" value="" class="large-field" onkeypress="return xFunc_justNumber(event);" onchange="javascript:xFunc_getCEP( this.value );"/></td>
				<!--td><input type="text" name="postcode" id="cep" value="" class="large-field" maxlength="8" onblur="consultacep(this.value)" /><span style="color:#FF0000;">*Insira somente números</span></td-->
			</tr>
			<div style="display:none;">
			<?php if ($company_id_display) { ?>
			<tr style="display:none;">
				<td>
				<?php echo $entry_company_id; ?> <?php if ($company_id_required) { ?><span class="required">*</span><?php } ?>
				</td>
				<td style="display:none;"><input type="text" name="company_id" value="" class="large-field" /></td>
			</tr>
			<?php } ?>
			</div>
			<?php if ($tax_id_display) { ?>
			<tr>
				<td><?php echo $entry_tax_id; ?> <?php if ($tax_id_required) { ?><span class="required">*</span><?php } ?></td>
				<td><input type="text" name="tax_id" value="" class="large-field" /></td>
			</tr>
			<?php } ?>
			<tr>
				<td><?php echo $entry_address_1; ?> <span class="required">*</span></td>
				<td><input type="text" name="address_1" id="logradouro"  value=""  class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_company; ?></td>
				<td><input type="text" name="company" value="" class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_address_2; ?></td>
				<td><input type="text" name="address_2" value="" id="bairro" class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_city; ?> <span class="required">*</span></td>
				<td><input type="text" name="city" value="" id="localidade"  class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_country; ?> <span class="required">*</span></td>
				<td><select name="country_id" class="large-field">
						<option value=""><?php echo $text_select; ?></option>
						<?php foreach ($countries as $country) { ?>
						<?php if ($country['country_id'] == $country_id) { ?>
						<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
						<?php } ?>
						<?php } ?>
					</select></td>
			</tr>
			<tr>
				<td><?php echo $entry_zone; ?> <span class="required">*</span></td>
				<td><select name="zone_id" class="large-field">
					</select></td>
			</tr>
		</table>
	</div>
</div>

<div class="buttons">
	<div class="right">
		<input type="button" value="<?php echo $button_continue; ?>" id="button-payment-address" class="button dark-bt" />
	</div>
</div>
<script type="text/javascript"><!--
$('#payment-address input[name=\'payment_address\']').live('change', function() {
	if (this.value == 'new') {
		$('#payment-existing').hide();
		$('#payment-new').show();
		$('.box-form').next().addClass('payment-new');
		
		$('#payment-new select').each(function(){
			if($(this).parent().hasClass('select') == false){
				$(this).wrap("<span class='select'></span>");
				$(this).addClass('select-menu');
			}
		});
	} else {
		$('#payment-existing').show();
		$('#payment-new').hide();
		$('.box-form').next().removeClass('payment-new');
	}
});
//--></script> 


<script type="text/javascript"><!--

$('#payment-address select[name=\'country_id\']').bind('change', function() {
	if (this.value == '') return;
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#payment-address select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#payment-postcode-required').show();
			} else {
				$('#payment-postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('#payment-address select[name=\'zone_id\']').html(html);
			
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#payment-address select[name=\'country_id\']').trigger('change');
//--></script>