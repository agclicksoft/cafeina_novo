
function verificarData(){

$("#verificar").click(function(){
  var datepicker = $("#datepicker").val();
  var hora = $("#hora").val();
  var minuto = $("#minuto").val();
  var loja = $("#lojasel").val();
  //var loja = $("input[name='loja']:checked").val();
  console.log("loja = " + loja);
  
  //alert(datepicker);


  // inicio uma requisição
  $.ajax({
   // url para o arquivo json.php
   url : "catalog/controller/module/data_hora.php",
   //Método de envio
   type: 'POST',
   // dataType json
   dataType : "json",

   data : { 'datepicker':datepicker, 'hora':hora, 'minuto':minuto, 'loja':loja },
   // função para de sucesso
   success : function(data){

    //console.log(data);
    if(data['Status'] == '0'){
     $("#retorno").show("slow").text("Você precisa selecionar uma data.");
     setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
     $("#buttoncont").hide("slow");
     $("#selecioneenvio").show("slow");
    }else if(data['Status'] == '1') {
     $("#retorno").show("slow").text("Horário mínimo permitido para o dia "+data['datepicker'] + " é às " + data['hora']);
     setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
     $("#buttoncont").hide("slow");
     $("#selecioneenvio").show("slow");
    } else if(data['Status'] == '2') {
     $("#retorno").show("slow").text("Sucesso! Data e hora selecionada: "+data['datepicker'] + " às " + data['hora']);
     setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
     $("#buttoncont").show("slow");
     $("#selecioneenvio").hide("slow");
     var data_hora = data['datepicker'] + " " + data['hora'];
     //console.log(data_hora);
     $("#data_hora").val(data_hora);
    } else if(data['Status'] == '3'){
      $("#retorno").show("slow").text("Você só pode escolher na loja "+ data['loja'] + " depois das "+ data['horaminima']);
     setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
     $("#buttoncont").hide("slow");
     $("#selecioneenvio").show("slow");
    }else if(data['Status'] == '4'){
      $("#retorno").show("slow").text("Você só pode escolher no dia "+ data['datepicker'] + " até às "+ data['horamaxima']);
     setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
     $("#buttoncont").hide("slow");
     $("#selecioneenvio").show("slow");
    }else{
      $("#retorno").show("slow").text("Você só pode escolher no dia "+ data['datepicker'] + " depois das "+ data['horaminima']);
     setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
     $("#buttoncont").hide("slow");
     $("#selecioneenvio").show("slow");
    }
   }
  });//termina o ajax
});

}

function verificaVespera(){
  $("#verificar").click(function(){
    var datepicker = $("#datepicker").val();
    var parte = $("#parte").val();
    var loja = $("#lojasel").val();
    //var loja = $("input[name='loja']:checked").val();
    console.log("loja = " + loja);
    
    //alert(datepicker);


    // inicio uma requisição
    $.ajax({
     // url para o arquivo json.php
     url : "catalog/controller/module/data_hora_vespera.php",
     //Método de envio
     type: 'POST',
     // dataType json
     dataType : "json",

     data : { 'datepicker':datepicker, 'parte':parte, 'loja':loja },
     // função para de sucesso
     success : function(data){

      //console.log(data);
      if(data['Status'] == 'erro'){
       $("#retorno").show("slow").text("Você só pode escolher no dia " + data['datepicker'] + " na parte da " + data['parte']);
       setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
       $("#buttoncont").hide("slow");
       $("#selecioneenvio").show("slow");
      }else{
        $("#retorno").show("slow").text("Sucesso! Data selecionada: "+data['datepicker'] + " na parte da " + data['parte']);
        setTimeout(function() { $("#retorno").hide("slow"); }, 6000);
        $("#buttoncont").show("slow");
        $("#selecioneenvio").hide("slow");
        var data_hora = data['datepicker'] + " " + data['parte'];
        //console.log(data_hora);
        $("#data_hora").val(data_hora);
      }
     }
    });//termina o ajax
  });
}