<div class="box-form">
	<?php if ($addresses) { ?>
	
	<input type="radio" name="shipping_address" value="existing" id="shipping-address-existing" checked="checked" />
	<label for="shipping-address-existing"><strong><?php echo $text_address_existing; ?></strong></label>
	<div id="shipping-existing">
		<select name="address_id" style="width: 100%; margin-bottom: 15px;" size="5">
			<?php foreach ($addresses as $address) { ?>
			<?php if ($address['address_id'] == $address_id) { ?>
			<option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
			<?php } else { ?>
			<option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
			<?php } ?>
			<?php } ?>
		</select>
	</div>

	

	<script>

   		function consultacep2(cep2){
	      	$('#logradouro2').val(null);
	      	$('#bairro2').val(null);
	      	$('#localidade2').val(null);
	    	$('#logradouro2').attr('placeholder','Carregando...');
	      	$('#bairro2').attr('placeholder','Carregando...');
	      	$('#localidade2').attr('placeholder','Carregando...');

	      cep2 = cep2.replace(/\D/g,"")
	      url="http://cep.correiocontrol.com.br/"+cep2+".js"
	      s=document.createElement('script')
	      s.setAttribute('charset','utf-8')
	      s.src=url
	      document.querySelector('head').appendChild(s)
	    }
	 
	    function correiocontrolcep(valor2){
	      if (valor2.erro) {
	      	$('#logradouro2').removeAttr('placeholder');
	      	$('#bairro2').removeAttr('placeholder');
	      	$('#localidade2').removeAttr('placeholder');
	      	$('#logradouro2').val(null);
	      	$('#bairro2').val(null);
	      	$('#localidade2').val(null);
	        alert('Cep não encontrado');       
	        return;
	      };

	      document.getElementById('logradouro2').value=valor2.logradouro
	      document.getElementById('bairro2').value=valor2.bairro
	      document.getElementById('localidade2').value=valor2.localidade
	      //document.getElementById('uf').value=valor.uf
	    }
    </script>

	<div>
		<input type="radio" name="shipping_address" value="new" id="shipping-address-new" />
		<label for="shipping-address-new"><strong><?php echo $text_address_new; ?></strong></label>
	</div>
	<?php } ?>
	<div id="shipping-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
		<table class="form">
			<tr>
				<td><?php echo $entry_firstname; ?> <span class="required">*</span></td>
				<td><input type="text" name="firstname" value="" class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_lastname; ?> <span class="required">*</span></td>
				<td><input type="text" name="lastname" value="" class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_postcode; ?> <span class="required" id="shipping-postcode-required">*</span></td>
				<td><input type="text" name="postcode" value="" onblur="consultacep2(this.value)" id="cep2" class="large-field" /><span style="color:#FF0000;">*Insira somente números</span></td>
			</tr>
			<tr>
				<td><?php echo $entry_address_1; ?> <span class="required">*</span></td>
				<td><input type="text" disabled="disabled" name="address_1" value="" id="logradouro2" class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_company; ?></td>
				<td><input type="text" name="company" value="" class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_address_2; ?></td>
				<td><input type="text" disabled="disabled" name="address_2" value="" id="bairro2" class="large-field" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_city; ?> <span class="required">*</span></td>
				<td><input type="text" disabled="disabled" name="city" value="" id="localidade2" class="large-field" /></td>
			</tr>
			<tr style="display:none;">
				<td><?php echo $entry_country; ?> <span class="required">*</span></td>
				<td><select name="country_id" class="large-field">
						<option value=""><?php echo $text_select; ?></option>
						<?php foreach ($countries as $country) { ?>
						<?php if ($country['country_id'] == $country_id) { ?>
						<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
						<?php } ?>
						<?php } ?>
					</select></td>
			</tr>
			<tr>
				<td><?php echo $entry_zone; ?> <span class="required">*</span></td>
				<td><select disabled="disabled" name="zone_id" class="large-field">
				<?php 
				if ($entry_zone){ ?>
					<option value="458" selected="selected">Rio de Janeiro</option>
			<?php	}
			?>
					</select></td>
			</tr>
		</table>
	</div>
</div>

<div class="buttons">
	<div class="right">
		<input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-address" class="button dark-bt" />
	</div>
</div>
<script type="text/javascript"><!--


$('#shipping-address input[name=\'shipping_address\']').live('change', function() {
	if (this.value == 'new') {
		$('#shipping-existing').hide();
		$('#shipping-new').show();
		$('.box-form').next().addClass('shipping-new');
		
		$('#shipping-new select').each(function(){
			if($(this).parent().hasClass('select') == false){
				$(this).wrap("<span class='select'></span>");
				$(this).addClass('select-menu');
			}
		});
		
	} else {
		$('#shipping-existing').show();
		$('#shipping-new').hide();
		$('.box-form').next().removeClass('shipping-new');
	}
});
//--></script> 


<script type="text/javascript"><!--

$('#shipping-address select[name=\'country_id\']').bind('change', function() {
	if (this.value == '') return;
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#shipping-address select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#shipping-postcode-required').show();
			} else {
				$('#shipping-postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('#shipping-address select[name=\'zone_id\']').html(html);
			
			
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#shipping-address select[name=\'country_id\']').trigger('change');
//--></script>