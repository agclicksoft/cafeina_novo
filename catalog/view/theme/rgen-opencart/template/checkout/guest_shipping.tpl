	<script>
	    function consultacep2(cep2){
	      	$('#logradouro2').val(null);
	      	$('#bairro2').val(null);
	      	$('#localidade2').val(null);
	    	$('#logradouro2').attr('placeholder','Carregando...');
	      	$('#bairro2').attr('placeholder','Carregando...');
	      	$('#localidade2').attr('placeholder','Carregando...');

	      cep2 = cep2.replace(/\D/g,"")
	      url="http://cep.correiocontrol.com.br/"+cep2+".js"
	      s=document.createElement('script')
	      s.setAttribute('charset','utf-8')
	      s.src=url
	      document.querySelector('head').appendChild(s)
	    }
	 
	    function correiocontrolcep(valor2){
	      if (valor2.erro) {
	      	$('#logradouro2').removeAttr('placeholder');
	      	$('#bairro2').removeAttr('placeholder');
	      	$('#localidade2').removeAttr('placeholder');
	      	$('#logradouro2').val(null);
	      	$('#bairro2').val(null);
	      	$('#localidade2').val(null);
	        alert('Cep não encontrado');       
	        return;
	      };

	      document.getElementById('logradouro2').value=valor2.logradouro
	      document.getElementById('bairro2').value=valor2.bairro
	      document.getElementById('localidade2').value=valor2.localidade
	      //document.getElementById('uf').value=valor.uf
	    }
    </script>

<table class="form">
	<tr>
		<td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
		<td><input type="text" name="firstname" value="<?php echo $firstname; ?>" class="large-field" /></td>
	</tr>
	<tr>
		<td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
		<td><input type="text" name="lastname" value="<?php echo $lastname; ?>" class="large-field" /></td>
	</tr>
	<tr>
		<td><span class="required" id="shipping-postcode-required">*</span> <?php echo $entry_postcode; ?></td>
		<td><input type="text" name="postcode" value="<?php echo $postcode; ?>" class="large-field" id="cep2" onblur="consultacep2(this.value)"/></td>
	</tr>
	<tr>
		<td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
		<td><input type="text" disabled="disabled" name="address_1" value="<?php echo $address_1; ?>" class="large-field" id="logradouro2" onblur="consultacep();"/></td>
	</tr>
	<tr>
		<td><?php echo $entry_company; ?></td>
		<td><input type="text" name="company" value="<?php echo $company; ?>" class="large-field" /></td>
	</tr>
	<tr>
		<td><?php echo $entry_address_2; ?></td>
		<td><input type="text" disabled="disabled" name="address_2" value="<?php echo $address_2; ?>" class="large-field" id="bairro2"/></td>
	</tr>
	<tr>
		<td><span class="required">*</span> <?php echo $entry_city; ?></td>
		<td><input type="text" disabled="disabled" name="city" value="<?php echo $city; ?>" class="large-field" id="localidade2"/></td>
	</tr>
	<tr style="display:none;">
		<td><span class="required">*</span> <?php echo $entry_country; ?></td>
		<td><select name="country_id" class="large-field">
				<option value=""><?php echo $text_select; ?></option>
				<?php foreach ($countries as $country) { ?>
				<?php if ($country['country_id'] == $country_id) { ?>
				<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
				<?php } ?>
				<?php } ?>
			</select></td>
	</tr>
	<tr>
		<td style="display:none;"><span class="required">*</span> <?php echo $entry_zone; ?></td>
		<td style="display:none;"><select disabled="disabled" name="zone_id" class="large-field">
			<?php 
				if ($entry_zone){ ?>
					<option value="458" selected="selected">Rio de Janeiro</option>
			<?php	}
			?>

			</select></td>
	</tr>
</table>
<br />
<div class="buttons">
	<div class="right">
		<input type="button" value="<?php echo $button_continue; ?>" id="button-guest-shipping" class="button dark-bt" />
	</div>
</div>
<script type="text/javascript"><!--

$('#shipping-address select[name=\'country_id\']').bind('change', function() {
	if (this.value == '') return;
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#shipping-address select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#shipping-postcode-required').show();
			} else {
				$('#shipping-postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('#shipping-address select[name=\'zone_id\']').html(html);
			
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#shipping-address select[name=\'country_id\']').trigger('change');

//--></script> 