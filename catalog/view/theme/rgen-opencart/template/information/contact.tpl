<?php echo $header; ?>

<div id="content">
	
	<!--CONTENT LEFT -->
	<?php echo $column_left; ?>
	
	<!--CONTENT RIGHT -->
	<?php echo $column_right; ?>
	
	<!--PAGE CONTENT WRAPPER -->
	<div class="content-body contactus">
		
		<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<span><?php echo $breadcrumb['separator']; ?></span><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
		</div>
		
		<h1 class="page-heading"><strong><?php echo $heading_title; ?></strong></h1>
		
		<?php echo $content_top; ?>
		
		
			<table>
				<tr>
					<td class="left">
						<div class="contact-info contact-info-1">
							<div class="content">

								<?php if ($telephone) { ?>
								<h2 class="header-3"><?php echo $text_telephone; ?><?php echo ' '.$telephone; ?></h2>
								<?php } ?>
								
								<h2 class="header-3"><?php echo $text_address; ?></h2>
								<h3 class="header-4"><?php echo 'Cafeína Botafogo - (21) 2237-9160'; ?></h3>
								<?php echo '<b>Botafogo Praia Shopping, loja 137</b><br/>Diariamente das 09h às 23h'; ?><p class="header-3"/>

								<h3 class="header-4"><?php echo 'Cafeína Copa I - (21) 2547-8651'; ?></h3>
								<?php echo '<b>Rua Constante Ramos, 44</b><br/>Diariamente das 08h às 23h30min'; ?><p class="header-3"/>

								<h3 class="header-4"><?php echo 'Cafeína Copa II - (21) 2547-4390'; ?></h3>
								<?php echo '<b>Rua Barata Ribeiro, 507</b><br/>Diariamente das 08h às 23h'; ?><p class="header-3"/>


								<h3 class="header-4"><?php echo 'Cafeína Ipanema - (21) 2521-2194'; ?></h3>
								<?php echo '<b>Rua Farme de Amoedo, 43</b><br/>Diariamente das 08h às 23h'; ?><p class="header-3"/>

								<h3 class="header-4"><?php echo 'Cafeína Leblon - (21) 2259-4224'; ?></h3>
								<?php echo '<b>Rio Design Leblon, loja 119</b><br/>Diariamente das 10h às 23h sábado, domingo e feriados de 09h às 23h'; ?><p class="header-3"/>

								<h3 class="header-4"><?php echo 'Cafeína Nova América - (21) 2218-8121'; ?></h3>
								<?php echo '<b>Shopping Nova América, loja 108 - 1º piso (Entrada A)</b><br/>Diariamente das 10h às 22h domingos e feriados de 11h as 22h'; ?><p class="header-3"/>

								<h3 class="header-4"><?php echo 'Cafeína Rio Sul - (21) 2543-5943'; ?></h3>
								<?php echo '<b>Shopping Rio Sul, loja 401</b><br/>Diariamente das 10h às 22h domingo e feriado de 12h as 21h'; ?><p class="header-3"/>


								<!--
								<?php if ($telephone) { ?>
								<h2 class="header-3"><?php echo $text_telephone; ?></h2>
								<strong><?php echo $telephone; ?><br /></strong>
								<br /><br /><br />
								<?php } ?> -->
								
								
								<?php if ($fax) { ?>
								<h2 class="header-3"><?php echo $text_fax; ?></h2>
								<strong><?php echo $fax; ?></strong>
								<?php } ?>
								
							</div>
						</div>
					</td>
					<td class="right box-form">
					<form id="form" onsubmit="return validateExtension(this);" action="catalog/view/theme/rgen-opencart/template/information/envio.php" method="post" enctype="multipart/form-data">
						<h2 class="header-3"><?php echo $text_contact; ?></h2>
						<div class="content">
							
							<table>
								<tr>
									<td>
										<label for="name"><?php echo $entry_name; ?></label>
										<input type="text" name="name" style="margin-right:20px" value="<?php echo $name; ?>" />
										<?php if ($error_name) { ?>
										<span class="error"><?php echo $error_name; ?></span>
										<?php } ?>
									</td>
									<td>
										<label for="email"><?php echo $entry_email; ?></label>
										<input type="text" name="email" value="<?php echo $email; ?>" />
										<?php if ($error_email) { ?>
										<span class="error"><?php echo $error_email; ?></span>
										<?php } ?>	
									</td>
								</tr>
								<tr>
									<td>
										<label for="phone">Telefone:</label>
										<input type="text" name="phone" value="" />
									</td>
									<td>
										<label for="arquivo">Anexar Curriculo:</label>
										<input type="file" name="arquivo" value="" />
									</td>
								</tr>
							</table>
							
							<label for="enquiry"><?php echo $entry_enquiry; ?></label>
							<textarea name="enquiry" cols="30" rows="5"><?php echo $enquiry; ?></textarea>
							<?php if ($error_enquiry) { ?>
							<span class="error"><?php echo $error_enquiry; ?></span>
							<?php } ?>
							
							<label for="captcha"><?php echo $entry_captcha; ?></label>
							<input type="text" name="captcha" value="<?php echo $captcha; ?>" />
							<img src="index.php?route=information/contact/captcha" class="captchaimg" alt="" />
							<?php if ($error_captcha) { ?>
							<span class="error"><?php echo $error_captcha; ?></span>
							<?php } ?>
						</div>
						<input type="submit" value="<?php echo $button_continue; ?>" class="button dark-bt" />
					</form>
					</td>
				</tr>
			</table>		
	</div>

	<div class="clearfix"></div>
	<?php echo $content_bottom; ?>

</div>

</div>

<link href="catalog/view/theme/rgen-opencart/stylesheet/jquery-confirm.css" rel="stylesheet">
<script type="text/javascript" src="catalog/view/theme/rgen-opencart/js/jquery-confirm.js"></script>
<script type="text/javascript">
	var _validFileExtensions = [".pdf", ".doc", ".docx", ".word"];    

	function validateExtension(oForm) {
	    var arrInputs = oForm.getElementsByTagName("input");
	    for (var i = 0; i < arrInputs.length; i++) {
	        var oInput = arrInputs[i];
	        if (oInput.type == "file") {
	            var sFileName = oInput.value;
	            if (sFileName.length > 0) {
	                var blnValid = false;
	                for (var j = 0; j < _validFileExtensions.length; j++) {
	                    var sCurExtension = _validFileExtensions[j];
	                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
	                        blnValid = true;
	                        break;
	                    }
	                }
	                
	                if (!blnValid) {
	                   // alert("O arquivo " + sFileName + " é Inválido, as extensões permitidas são: " + _validFileExtensions.join(", "));
	                    $.alert({
	                        title: 'Extensão Inválida!',
	                        content: 'As extensões permitidas são: pdf, doc, docx, word.',                        
	                    });
	                    return false;
	                }
	            }

	        }
	    }
	  
	    return true;
	}
</script>

<?php echo $footer; ?>
