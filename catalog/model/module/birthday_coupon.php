<?php
class ModelModuleBirthdayCoupon extends Model {
	
	public function addBirthday($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "customer_birthday
				SET customer_id = '" . (int)$this->customer->getId() . "',
					year        = '" . (int)$data['birth_year'] . "',
					month       = '" . (int)$data['birth_month'] . "',
					day         = '" . (int)$data['birth_day'] . "'";
					
		$this->db->query($sql);
	}
	
	public function editBirthday($data) {
		$sql = "UPDATE " . DB_PREFIX . "customer_birthday
				SET year        = '" . (int)$data['birth_year'] . "',
					month       = '" . (int)$data['birth_month'] . "',
					day         = '" . (int)$data['birth_day'] . "'
				WHERE customer_id = '" . (int)$this->customer->getId() . "'";
					
		$this->db->query($sql);
	}
	
	public function isBirthdayAdded() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_birthday WHERE customer_id = '" . (int)$this->customer->getId() . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getBirthYear() {
		$sql = "SELECT year FROM " . DB_PREFIX . "customer_birthday WHERE customer_id = '" . (int)$this->customer->getId() . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['year'];
	}
	
	public function getBirthMonth() {
		$sql = "SELECT month FROM " . DB_PREFIX . "customer_birthday WHERE customer_id = '" . (int)$this->customer->getId() . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['month'];
	}
	
	public function getBirthDay() {
		$sql = "SELECT day FROM " . DB_PREFIX . "customer_birthday WHERE customer_id = '" . (int)$this->customer->getId() . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['day'];
	}
	
	public function getCustomersCelebratedToday() {
		$sql = "SELECT c.* FROM " . DB_PREFIX . "customer c
				LEFT JOIN " . DB_PREFIX . "customer_birthday cb ON (c.customer_id = cb.customer_id)
				WHERE cb.month = MONTH(NOW()) AND cb.day = DAY(NOW()) 
		        AND cb.customer_id NOT IN (SELECT customer_id FROM " . DB_PREFIX . "customer_birthday_history WHERE year = YEAR(NOW()))";
		
		$query = $this->db->query($sql);
			
		return $query->rows;	
	}
	
	public function addCoupon($customer_info, $coupon_code){
	
		$sql = "INSERT INTO " . DB_PREFIX . "coupon 
				SET name          ='" . $this->db->escape('HB ' . $customer_info['firstname'] . ' ' . $customer_info['lastname']) . "',
				    code          ='" . $this->db->escape($coupon_code) . "',
					type          ='" . $this->db->escape($this->config->get('birthday_coupon_type')) . "',
					discount      ='" . (float)$this->config->get('birthday_coupon_discount') . "',
					logged        = 0,
					shipping      = 0,
					total         ='" . (float)$this->config->get('birthday_coupon_total') . "',
					date_start    = CURRENT_DATE(),
					date_end      = DATE_ADD(CURRENT_DATE(), INTERVAL " . (int)$this->config->get('birthday_coupon_validity_days') . " DAY),
					uses_total    = 1,
					uses_customer = 1,
					status        = 1,
					date_added    = NOW()";

		$query = $this->db->query($sql);		
	}
	
	public function addHistory($customer_id, $coupon_code) {
		$sql = "INSERT INTO " . DB_PREFIX . "customer_birthday_history 
				SET customer_id   ='" . (int)$customer_id . "',
				    year          = YEAR(NOW()),
					code          ='" . $this->db->escape($coupon_code) . "',
					type          ='" . $this->db->escape($this->config->get('birthday_coupon_type')) . "',
					discount      ='" . (float)$this->config->get('birthday_coupon_discount') . "',
					total         ='" . (float)$this->config->get('birthday_coupon_total') . "',
					validity      = DATE_ADD(CURRENT_DATE(), INTERVAL " . (int)$this->config->get('birthday_coupon_validity_days') . " DAY),
					date_added    = NOW()";

		$query = $this->db->query($sql);
	}
	
	public function getHappyBirthdayCouponsSentToday() {
		$sql = "SELECT cbh.*, CONCAT(c.firstname, ' ', c.lastname) as customer_name FROM " . DB_PREFIX . "customer_birthday_history cbh
				LEFT JOIN " . DB_PREFIX . "customer c ON (cbh.customer_id = c.customer_id)
				WHERE DATE(cbh.date_added) = DATE(NOW())";
				
		$query = $this->db->query($sql);

		return $query->rows;	
	}
}
?>