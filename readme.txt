Opencart - Date & Time Picker with Print Shipping Module! 

Developer's : Dude & The Path.
========================================

Original Developer "The Path" made a Specific Date Picker Shipping Module.

I Dude has updated Original Mod to "DATE & Time Picker With Print on Invoice. 

And I have a permission from him.


Supported OpenCart Versions:
============================
v1.5.2.*



What does it do:
================
The Date and Time Picker Shipping module allows people to choose their own delivery date and time from a pop out calendar during the checkout process. The date chosen by the customer is displayed in the order invoice in the admin panel and its also included in the email to the store owner.

Date & Time Picker Shipping module uses JQuery UI to display the pop out calendar, this creates an easy and fool proof way of obtaining the delivery date from the user.



Main features:
==============
  * Cool JQuery pop out calendar allowing easy selection of delivery date from the user/shopper
  * Delivery date displayed in admin order invoice and store owner email



How to install it:
==================
1) Unzip and upload the contents to the root directory of your OpenCart installation, preserving directory structure
2) From the admin menu, go to 'Admin->Users->User Groups'. Edit Top Administrator.
3) Find and check the entries for shiping/specificdate in both modify and access. save.
4) From the admin menu, go to 'Extensions->Shipping'.
5) Install the module, and click edit to configure.



Overwritten Files:
==================
catalog/controller/checkout/shipping_method.php
catalog/model/total/shipping.php
catalog/view/theme/default/template/checkout/checkout.tpl
catalog/view/theme/default/template/checkout/shipping_method.tpl

Please make sure and make backups, just incase ;)



Support Thread:
===============
http://forum.opencart.com/



License:
=========
This software is free. 
Contact ThePath @ info@thepathenterprises.com 

Contact Dude @ thehoax123@yahoo.com